package FourInARow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import FourInARow.FourInARow.FourBoard;
import chess.board.BB;
import chess.misc.Bits;
import game.Game;
import game.GameBoard;
import ml.FeatureFunction;
import ml.GameLearner;
import ml.GameLearner.Builder;

public class FourInARow implements Game<Integer, FourBoard> {

	private static FourInARow instance = null;

	private FourInARow() {
		// singleton class
	}

	public static class FourBoard implements GameBoard<Integer, FourBoard> {

		// 'random' used for generating zobrist keys.
		private static Random RANDOM = new Random(1238238132);

		private static final int MAX_PLAYERS = 2;
		private static final int MAX_SQUARES = 42;

		private static long FOUR_BOARD_MASK = 0x7f7f7f7f7f7fl;

		private static List<List<Integer>> MOVE_LIST_LOOKUP = IntStream.range(0, 128).mapToObj(FourBoard::toMoveList)
				.collect(Collectors.toList());
		private static int[] INDEX_LOOKUP = getIndexLookup();
		private static long[] ZOBRIST_KEYS = IntStream.range(0, 2 * MAX_SQUARES).mapToLong(i -> RANDOM.nextLong())
				.toArray();

		private int color = 0;
		private final long[] bitboards = new long[2];
		private long hash = 0l;
		private boolean isWin = false;
		private int movesPlayed = 0;

		private static int[] getIndexLookup() {
			int[] result = new int[64];
			AtomicInteger counter = new AtomicInteger();
			Bits.withEachSetBit(FOUR_BOARD_MASK, i -> result[i] = counter.getAndIncrement());
			return result;
		}

		private static List<Integer> toMoveList(int mask) {
			List<Integer> result = new ArrayList<Integer>(Integer.bitCount(mask));
			Bits.withEachSetBit(mask, result::add);
			return Collections.unmodifiableList(result);
		}

		public FourBoard() {
		}

		public FourBoard(FourBoard other) {
			System.arraycopy(other.bitboards, 0, bitboards, 0, MAX_PLAYERS);
			this.color = other.color;
			this.hash = other.hash;
			this.isWin = other.isWin;
			this.movesPlayed = other.movesPlayed;
		}

		@Override
		public long longhash() {
			return hash;
		}

		@Override
		public List<Integer> getMoves() {
			long free = FOUR_BOARD_MASK & ~(bitboards[0] | bitboards[1]);
			free >>>= 8 * 5;
			return new ArrayList<>(MOVE_LIST_LOOKUP.get((int) free));

		}

		@Override
		public List<Integer> getVariation() {
			throw new UnsupportedOperationException("Not (yet) implemented");
		}

		private boolean isWin(int square) {
			long bb = 1l << square | BB.queenAttacks(square, ~bitboards[color]) & bitboards[color];
			if (Long.bitCount(bb & BB.RANKS[square]) >= 4)
				return true;
			if (Long.bitCount(bb & BB.FILES[square]) >= 4)
				return true;
			if (Long.bitCount(bb & BB.NRTHWEST_TO_STHEAST[square]) >= 4)
				return true;
			if (Long.bitCount(bb & BB.STHWEST_TO_NRTHEAST[square]) >= 4)
				return true;
			return false;
		}

		@Override
		public boolean isLegalMove(Integer move) {
			long free = -1l & ~(bitboards[0] | bitboards[1]);
			free >>>= 8 * 5;
			return (free & 1l << move) != 0;
		}

		@Override
		public void pushMove(Integer m) {
			long mask = BB.FILES[m] & ~(occupied());
			int sq = Bits.bitscanForward(mask);
			this.isWin = isWin(sq);
			bitboards[color] |= 1l << sq;

			hash ^= ZOBRIST_KEYS[color * 42 + INDEX_LOOKUP[sq]];
			movesPlayed++;
			color ^= 1;
		}

		private long occupied() {
			return bitboards[0] | bitboards[1];
		}

		@Override
		public void popMove(Integer m) {
			color ^= 1;
			long mask = BB.FILES[m] & bitboards[color];
			int sq = Bits.bitscanBackwards(mask);
			bitboards[color] &= ~(1l << sq);
			hash ^= ZOBRIST_KEYS[color * 42 + INDEX_LOOKUP[sq]];
			isWin = false;
			movesPlayed--;

		}

		@Override
		public FourBoard copy() {
			return new FourBoard(this);
		}

		@Override
		public int getPlayerToMove() {
			return color;
		}

		@Override
		public game.GameBoard.GameState getGameState() {
			if (movesPlayed >= 42)
				return GameState.DRAW;
			else if (!isWin)
				return game.GameBoard.GameState.NO_END;
			else if (color == 0)
				return GameState.PLAYER2_WON;
			else
				return GameState.PLAYER1_WON;
		}

		@Override
		public void print() {
			System.out.println("////");
			for (int i = 5; i >= 0; i--) {
				StringBuilder b = new StringBuilder();
				for (int k = i * 8; k <= i * 8 + 6; k++) {
					int color = getColor(k);
					if (color == 0)
						b.append("R");
					else if (color == 1)
						b.append("T");
					else
						b.append("X");
				}
				System.out.println(b);
			}
			System.out.println("////");
		}

		private int getColor(int sq) {
			long mask = 1l << sq;
			if ((mask & bitboards[0]) != 0)
				return 1;
			if ((mask & bitboards[1]) != 0)
				return -1;
			return -0;
		}

		@Override
		public boolean isReversible() {
			return false;
		}

		@Override
		public boolean isRepetition() {
			return false;
		}

		@Override
		public Game<Integer, FourBoard> getGame() {
			return FourInARow.getInstance();
		}

		public double[] getFeature() {
			double[] result = new double[MAX_SQUARES];
			AtomicInteger c = new AtomicInteger();
			Bits.withEachSetBit(FOUR_BOARD_MASK, i -> {
				result[c.getAndIncrement()] = getColor(i);
			});
			return result;
		}

	}

	public static FourInARow getInstance() {
		if (instance == null)
			instance = new FourInARow();
		return instance;
	}

	@Override
	public FourBoard buildStartingBoard() {
		return new FourBoard();
	}

	@Override
	public FourBoard buildBoardFromString(String string) {
		throw new UnsupportedOperationException("Not implemented");
	}

	public static void main(String[] args) {

		learn();

	}

	@SuppressWarnings("serial")
	private static void learn() {
		Builder<Integer, FourBoard> builder = new GameLearner.Builder<>(FourInARow.getInstance());
		builder.setFeature(new FeatureFunction<FourInARow.FourBoard>() {

			@Override
			public double[] apply(FourBoard t) {
				return t.getFeature();
			}

			@Override
			public int features() {
				return FourBoard.MAX_SQUARES;
			}
		});

		GameLearner<?, ?> learner = builder.build();
		learner.learn();
	}
}
