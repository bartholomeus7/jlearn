package ann;

import org.jblas.DoubleMatrix;

import data.DataSet;

public abstract class ASkeletonTrainer implements AnnTrainer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3015945969980746222L;
	protected int its = 0;
	protected NeuralNetworkI trained = null;
	protected boolean parallel = true;

	public ASkeletonTrainer(NeuralNetworkI i) {
		this.setTrained(i);
	}

	public ASkeletonTrainer() {
	}

	@Override
	public AnnTrainer setTrained(NeuralNetworkI net) {
		this.trained = net;
		net.setTrainer(this);
		return this;
	}

	@Override
	public NeuralNetworkI getTrained() {
		return trained;
	}

	@Override
	public int getIterations() {
		return its;
	}

	@Override
	public void setParallel(boolean parallel) {
		this.parallel = parallel;
	}

	@Override
	public boolean getParallel() {
		return parallel;
	}

	protected DoubleMatrix[] getGradient(DataSet<?> data) {
		return trained.getDerative(data, parallel);

	}

}
