package ann;

import java.util.ArrayList;
import java.util.List;

import org.jblas.DoubleMatrix;

import data.DataSet;

public class AdaDelta extends ASkeletonTrainer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8641561811711834008L;

	public AdaDelta(NeuralNetworkI i) {
		super(i);
		setTrained(i);
	}

	public AdaDelta(double decay, double epsilon) {
		if (eps <= 0) {
			throw new IllegalArgumentException("Epsilon must be bigger than 0");
		}
		this.p = decay;
		this.eps = epsilon;
	}

	public AdaDelta() {

	}

	private static double DEFAULT_DECAY = 0.95d;
	private static double DEFAULT_EPSILON = 1e-3d;

	private double p = DEFAULT_DECAY;
	private double eps = DEFAULT_EPSILON;
	private double lrate = 1;

	private List<DoubleMatrix> Eg = new ArrayList<>();
	private List<DoubleMatrix> Ex = new ArrayList<>();

	@Override
	public AnnTrainer setTrained(NeuralNetworkI net) {
		this.trained = net;
		its = 0;
		Eg = new ArrayList<>();
		Ex = new ArrayList<>();
		for (DoubleMatrix m : net.getNullGradient()) {
			Eg.add(m);
			Ex.add(new DoubleMatrix().copy(m));
		}
		return this;
	}

	public AdaDelta setEpsilon(double eps) {
		this.eps = eps;
		return this;
	}

	public AdaDelta setLearningRate(double lrate) {
		this.lrate = lrate;
		return this;
	}

	@Override
	public void train(DataSet<?> data) {
		if (trained == null) {
			throw new IllegalStateException("No network to be trained is set.");
		}
		DoubleMatrix[] gradient = getGradient(data);
		int idx = 0;
		for (DoubleMatrix gr : gradient) {
			Matrices.ipOp(Eg.get(idx), gr, (eg, g) -> rm2(eg, g));
			DoubleMatrix xu = Matrices.generateMatrix(Ex.get(idx), Eg.get(idx), (ex, g) -> rms(ex) / rms(g)).muli(gr);
			Matrices.ipOp(Ex.get(idx), xu, (ex, x) -> rm2(ex, x));
			trained.update(xu.muli(lrate), idx);
			idx++;
		}
		its++;
	}

	private double rms(double x) {
		return Math.sqrt(x * x + eps);
	}

	private double rm2(double x1, double x2) {
		return p * x1 + (1 - p) * x2 * x2;
	}

}
