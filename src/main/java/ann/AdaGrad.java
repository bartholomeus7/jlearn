package ann;

import java.util.ArrayList;
import java.util.List;

import org.jblas.DoubleMatrix;

import data.DataSet;

public class AdaGrad extends ASkeletonTrainer {

	private static final long serialVersionUID = 2513407291393970494L;

	private static final double ADAGRAD_FUDGE = 1e-6d;
	private static final double ADAGRAD_STEP = 1e-2d;
	private List<DoubleMatrix> Eg = new ArrayList<>();

	@Override
	public AnnTrainer setTrained(NeuralNetworkI net) {
		this.trained = net;
		its = 0;
		Eg = new ArrayList<>();
		for (DoubleMatrix m : net.getNullGradient()) {
			Eg.add(m);
		}
		return this;
	}

	@Override
	public void train(DataSet<?> data) {
		if (trained == null) {
			throw new IllegalStateException("No network to be trained is set.");
		}
		DoubleMatrix[] gradient = getGradient(data);
		int idx = 0;
		for (DoubleMatrix gr : gradient) {
			Matrices.ipOp(Eg.get(idx), gr, (eg, g) -> eg + g * g);
			Matrices.ipOp(gr, Eg.get(idx), (g, eg) -> g * ADAGRAD_STEP / (ADAGRAD_FUDGE + Math.sqrt(eg)));
			trained.update(gr, idx++);
		}
		its++;
	}

}
