package ann;

import java.io.Serializable;

import data.DataSet;

public interface AnnTrainer extends Serializable {

	public void train(DataSet<?> data);

	public AnnTrainer setTrained(NeuralNetworkI net);

	public NeuralNetworkI getTrained();

	public int getIterations();

	public void setParallel(boolean parallel);

	public boolean getParallel();

}
