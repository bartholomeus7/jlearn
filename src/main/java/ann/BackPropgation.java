package ann;

import org.jblas.DoubleMatrix;

import data.DataSet;

public class BackPropgation extends ASkeletonTrainer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7915109266739041780L;

	public BackPropgation(NeuralNetworkI i) {
		super(i);
	}

	private static double DEFAULT_LEARNING_RATE = 0.7d;
	private static double DEFAULT_MOMENTUM = 0.15d;

	private double learningRate = DEFAULT_LEARNING_RATE;
	private double momentum = DEFAULT_MOMENTUM;

	private DoubleMatrix[] lastGradient;

	public double getLearningRate() {
		return learningRate;
	}

	public BackPropgation setLearningRate(double learningRate) {
		this.learningRate = learningRate;
		return this;
	}

	private DoubleMatrix[] calcUpdate(DoubleMatrix[] gradient) {
		if (lastGradient == null) {
			for (DoubleMatrix m : gradient) {
				m.mul(learningRate);
			}
		} else {
			for (int i = 0; i < gradient.length; i++) {
				gradient[i] = lastGradient[i].muli(momentum).addi(gradient[i].muli(learningRate));
			}
		}
		return lastGradient = gradient;
	}

	@Override
	public void train(DataSet<?> data) {
		DoubleMatrix[] gradient = getGradient(data);
		int idx = 0;
		gradient = calcUpdate(gradient);
		for (DoubleMatrix gr : gradient) {
			trained.update(gr.muli(learningRate), idx);
			idx++;
		}
		its++;
	}

}
