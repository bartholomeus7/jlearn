package ann;

public interface C1Function {

	public double func(double x);

	public double derative(double x);

}
