package ann;

public enum ErrorFunctions implements C1_2Function {
	LINEAR {
		@Override
		public double func(double x, double y) {
			return x - y;
		}

		@Override
		public double derative(double x, double y) {
			return Math.signum(x - y);
		}
	},
	QUADRATIC {
		@Override
		public double func(double x, double y) {
			return (x - y) * (x - y);
		}

		@Override
		public double derative(double x, double y) {
			return 0.5d *(x - y);
		}
	},

	;

}
