package ann;

import java.util.function.DoubleUnaryOperator;
import java.util.function.Supplier;

import org.jblas.DoubleMatrix;

public class Matrices {

	public static interface DoubleOperator {
		public double calc(double x, double y);
	}

	private Matrices() {
		throw new RuntimeException("Static utility class");
	}

	public static DoubleMatrix applyToMatrix(DoubleMatrix matrix, DoubleUnaryOperator func) {
		for (int i = 0; i < matrix.length; i++) {
			matrix.put(i, func.applyAsDouble(matrix.get(i)));
		}
		return matrix;
	}

	public static DoubleMatrix ipOp(DoubleMatrix inplace, DoubleMatrix other, DoubleOperator func) {
		for (int i = 0; i < inplace.length; i++) {
			inplace.put(i, func.calc(inplace.get(i), other.get(i)));
		}
		return inplace;
	}

	public static DoubleMatrix op(DoubleMatrix other, DoubleUnaryOperator func) {
		return applyToMatrix(new DoubleMatrix().copy(other), func);
	}

	public static DoubleMatrix generateMatrix(DoubleMatrix d1, DoubleMatrix d2, DoubleOperator func) {
		d1.assertSameSize(d2);
		DoubleMatrix result = new DoubleMatrix(d1.rows, d1.columns);
		for (int i = 0; i < d1.length; i++) {
			result.put(i, func.calc(d1.get(i), d2.get(i)));
		}
		return result;
	}

	public static void addInPlace(DoubleMatrix[] inplace, DoubleMatrix[] other) {
		for (int i = 0; i < inplace.length; i++) {
			inplace[i].addi(other[i]);
		}
	}

	public static DoubleMatrix generateMatrix(int rows, int columns, Supplier<Double> supplier) {
		DoubleMatrix result = new DoubleMatrix(rows, columns);
		for (int i = 0; i < result.length; i++) {
			result.put(i, supplier.get());
		}
		return result;
	}

	public static DoubleMatrix[] forEachMatrixEachElement(DoubleMatrix[] matrices, DoubleUnaryOperator func) {
		for (int i = 0; i < matrices.length; i++) {
			Matrices.applyToMatrix(matrices[i], func);
		}
		return matrices;
	}



}
