package ann;

import java.io.Serializable;
import java.util.Arrays;
import java.util.function.Function;

import org.jblas.DoubleMatrix;

import data.DataSet;
import data.DataSet.DataRow;

public class MultiLayerPerceptron implements NeuralNetworkI, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -819613423252739845L;

	private static final Function<NeuralNetworkI, AnnTrainer> DEFAULT_TRAINER_FACTORY = n -> new AdaDelta()
			.setTrained(n);

	private final DoubleMatrix[] weights;
	private final DoubleMatrix[] biasWeights;
	private final C1Function[] transfers;
	private C1_2Function error = ErrorFunctions.QUADRATIC;
	private AnnTrainer trainer;
	private WeightInitSupplier wInit = WeightInits.GAUSSIAN;

	public MultiLayerPerceptron(int... layers) {
		weights = new DoubleMatrix[layers.length - 1];
		biasWeights = new DoubleMatrix[layers.length - 1];
		transfers = new C1Function[layers.length - 1];
		setTransferFunction(TransferFunction.TANH);
		for (int i = 0; i < layers.length - 1; i++) {
			int l1 = layers[i];
			int l2 = layers[i + 1];
			weights[i] = Matrices.generateMatrix(l2, l1, wInit.getWeightInit(l1, l2));
			biasWeights[i] = Matrices.generateMatrix(l2, 1, wInit.getWeightInit(l1, l2));
		}
		trainer = DEFAULT_TRAINER_FACTORY.apply(this);
	}

	public void setErrorFunction(C1_2Function error) {
		this.error = error;
	}

	private MultiLayerPerceptron(MultiLayerPerceptron other, int... newLayers) {
		this(newLayers);
		this.initWeights(WeightInits.NULL);
		this.trainer = other.trainer;
		System.arraycopy(other.transfers, 0, transfers, 0, other.transfers.length);
		for (int i = 0; i < weights.length; i++) {
			for (int k = 0; k < other.weights[i].columns; k++) {
				weights[i].putColumn(k,
						new DoubleMatrix(Arrays.copyOf(other.weights[i].getColumn(k).toArray(), weights[i].rows)));
			}
		}
		for (int i = 0; i < biasWeights.length; i++) {
			biasWeights[i] = new DoubleMatrix(Arrays.copyOf(other.biasWeights[i].toArray(), biasWeights[i].rows));
		}
	}

	public MultiLayerPerceptron expand(int... newLayers) {
		return new MultiLayerPerceptron(this, newLayers);
	}

	public MultiLayerPerceptron initWeights(WeightInitSupplier wInit) {
		for (int i = 0; i < weights.length; i++) {
			int l1 = weights[i].columns;
			int l2 = weights[i].rows;
			weights[i] = Matrices.generateMatrix(l2, l1, wInit.getWeightInit(l1, l2));
			biasWeights[i] = Matrices.generateMatrix(l2, 1, wInit.getWeightInit(l1, l2));
		}
		return this;
	}

	@Override
	public void setTransferFunction(C1Function func, int layer) {
		this.transfers[layer] = func;
	}

	@Override
	public void setTransferFunction(C1Function func) {
		Arrays.fill(transfers, func);
	}

	@Override
	public double[] calculate(double[] t) {
		DoubleMatrix f = new DoubleMatrix(t);
		for (int i = 0; i < weights.length; i++) {
			f = weights[i].mmul(f);
			f = f.addi(biasWeights[i]);
			f = Matrices.applyToMatrix(f, transfers[i]::func);
		}
		return f.toArray();
	}

	@Override
	public void train(DataSet<?> data, boolean parallel) {
		if (trainer.getTrained() != this)
			throw new IllegalStateException("Trainer has not been assigned to this network");
		trainer.train(data);
	}

	@Override
	public DoubleMatrix[] getDerative(DataSet<?> data, boolean parallel) {
		DoubleMatrix[] result = data.stream().parallel().unordered().map(i -> (DataRow) i).map(this::getDerative)
				.collect(this::getNullGradient, Matrices::addInPlace, Matrices::addInPlace);
		Arrays.stream(result).forEach(m -> m.muli(1d / data.size()));
		return result;
	}

	@SuppressWarnings("unused")
	private static DoubleMatrix toInputMatrix(DataSet<?> data) {
		int cols = data.getRow(0).getInput().length;
		DoubleMatrix input = new DoubleMatrix(cols, data.size());
		int count = 0;
		for (DataRow r : data) {
			input.putColumn(count, new DoubleMatrix(cols, 1, data.getRow(count++).getInput()));
		}
		return input;
	}

	@SuppressWarnings("unused")
	private static DoubleMatrix toOutputMatrix(DataSet<?> data) {
		int cols = data.getRow(0).getOutput().length;
		DoubleMatrix output = new DoubleMatrix(cols, data.size());
		int count = 0;
		for (DataRow r : data) {
			output.putColumn(count, new DoubleMatrix(cols, 1, data.getRow(count++).getOutput()));
		}
		return output;
	}

	@Override
	public DoubleMatrix[] getNullGradient() {
		DoubleMatrix[] result = new DoubleMatrix[weights.length + biasWeights.length];
		for (int i = 0; i < weights.length; i++) {
			result[i] = new DoubleMatrix(weights[i].rows, weights[i].columns);
		}
		for (int i = 0; i < biasWeights.length; i++) {
			result[i + weights.length] = new DoubleMatrix(biasWeights[i].length);
		}
		return result;
	}

	private DoubleMatrix[] getDerative(DataRow row) {
		return getDerative(row.getInput(), row.getOutput());
	}

	public DoubleMatrix[] getDerative(double[] input, double[] output) {
		DoubleMatrix[][] S = getActivation(input);
		DoubleMatrix[] summedInput = S[0];
		DoubleMatrix[] activation = S[1];
		DoubleMatrix netOut = activation[activation.length - 1];
		DoubleMatrix error = Matrices.generateMatrix(new DoubleMatrix(output), netOut, this.error::derative);
		DoubleMatrix[] delta = backPropagation(summedInput, error);
		DoubleMatrix[] derative = new DoubleMatrix[weights.length + biasWeights.length];

		for (int i = delta.length - 1; i >= 1; i--) {
			DoubleMatrix f = new DoubleMatrix(weights[i - 1].getRows(), weights[i - 1].getColumns());
			f.addiColumnVector(delta[i]);
			f.muliRowVector(activation[i - 1]);
			derative[i - 1] = f;
		}

		for (int i = biasWeights.length; i < derative.length; i++) {
			derative[i] = delta[1 + i - weights.length];
		}
		return derative;
	}

	public double getError(double[] input, double[] out) {
		double error = 0;
		double[] result = calculate(input);
		for (int i = 0; i < out.length; i++) {
			double e = result[i] - out[i];
			error += e * e;
		}
		return Math.sqrt(error) / out.length;
	}

	private DoubleMatrix[] backPropagation(DoubleMatrix[] input, DoubleMatrix error) {
		DoubleMatrix[] O = new DoubleMatrix[weights.length + 1];
		O[weights.length] = Matrices
				.applyToMatrix(new DoubleMatrix().copy(input[input.length - 1]), transfers[input.length - 2]::derative)
				.muli(error).transpose();
		for (int i = weights.length - 1; i >= 1; i--) {
			DoubleMatrix wxop1 = (O[i + 1]).mmul(weights[i]);
			DoubleMatrix fh = Matrices.applyToMatrix(input[i], transfers[i - 1]::derative);
			O[i] = wxop1.muli(fh);
		}
		return O;
	}

	public void printTests(DataSet<?> set) {
		System.out.println("------------------------");
		for (DataRow r : set) {
			double[] result = this.calculate(r.getInput());
			System.out.println(Arrays.toString(result) + " true: " + Arrays.toString(r.getOutput()));
			System.out.println("|||||");
		}
		System.out.println("Error=" + getError(set));
		System.out.println("------------------------");

		DoubleMatrix f = new DoubleMatrix(set.getRow(0).getInput());

		for (int i = 0; i < weights.length; i++) {
			f = weights[i].mmul(f);
			f = f.addi(biasWeights[i]);
			f = Matrices.applyToMatrix(f, transfers[i]::func);
		}

	}

	@Override
	public double getError(DataSet<?> dataSet) {
		double sumError = 0;
		for (DataRow r : dataSet) {
			double localError = getError(r.getInput(), r.getOutput());
			sumError += localError * localError;
		}
		return Math.sqrt(sumError) / dataSet.size();
	}

	private DoubleMatrix[][] getActivation(double[] input) {
		DoubleMatrix[][] S = new DoubleMatrix[2][weights.length + 1];
		S[1][0] = new DoubleMatrix(input);
		for (int i = 1; i < weights.length + 1; i++) {
			S[0][i] = weights[i - 1].mmul(S[1][i - 1]);
			S[0][i].addi(biasWeights[i - 1]);
			S[1][i] = Matrices.applyToMatrix(new DoubleMatrix().copy(S[0][i]), transfers[i - 1]::func);
		}
		return S;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append("|WEIGHTS|").append(System.lineSeparator());
		for (DoubleMatrix i : weights) {
			b.append(i.toString()).append(System.lineSeparator());
		}
		b.append("|BIAS|").append(System.lineSeparator());
		for (DoubleMatrix i : biasWeights) {
			b.append(i.toString()).append(System.lineSeparator());
		}
		return b.toString();
	}

	@Override
	public int getIntputSize() {
		return weights[0].columns;
	}

	@Override
	public int getOutputSize() {
		return weights[weights.length - 1].rows;
	}

	@Override
	public void setTrainer(AnnTrainer trainer) {
		this.trainer = trainer;
	}

	@Override
	public AnnTrainer getTrainer() {
		return trainer;
	}

	@Override
	public void update(DoubleMatrix update, int idx) {
		DoubleMatrix apply = idx >= weights.length ? biasWeights[idx - weights.length] : weights[idx];
		apply.addi(update);
	}

	@Override
	public int layers() {
		return weights.length - 1;
	}

	private DoubleMatrix[][] getActivationM(DoubleMatrix matrix) {
		DoubleMatrix[][] S = new DoubleMatrix[2][weights.length + 1];
		S[1][0] = matrix;
		for (int i = 1; i < weights.length + 1; i++) {
			S[0][i] = weights[i - 1].mmul(S[1][i - 1]);
			S[0][i].addColumnVector(biasWeights[i - 1]);
			S[1][i] = Matrices.applyToMatrix(new DoubleMatrix().copy(S[0][i]), transfers[i - 1]::func);
		}
		return S;
	}

	public static final DoubleMatrix doShit(DoubleMatrix target, DoubleMatrix first, DoubleMatrix second) {
		System.out.println("ROWS= " + first.rows);
		for (int i = 0; i < first.rows; i++) {
			for (int c = 0; c < target.columns; c++) {
				for (int r = 0; r < target.rows; r++) {
					target.put(r, c, target.get(r, c) + first.get(i, r) * second.get(r, i));
				}
			}
		}

		return target;
	}

	public DoubleMatrix[] getDerativeMatrix(DoubleMatrix input, DoubleMatrix output) {
		DoubleMatrix[][] S = getActivationM(input);
		DoubleMatrix[] summedInput = S[0];
		DoubleMatrix[] activation = S[1];
		DoubleMatrix netOut = activation[activation.length - 1];
		DoubleMatrix error = Matrices.generateMatrix(new DoubleMatrix().copy(output), netOut, this.error::derative);
		DoubleMatrix[] delta = backPropagation(summedInput, error);
		DoubleMatrix[] derative = new DoubleMatrix[weights.length + biasWeights.length];

		for (int i = delta.length - 1; i >= 1; i--) {
			DoubleMatrix f = new DoubleMatrix(weights[i - 1].getRows(), weights[i - 1].getColumns());
			/*
			 * System.out.println("FOO"); System.out.println(f.getColumns());
			 * System.out.println(f.rows); System.out.println("dc "
			 * +delta[i].getColumns()); System.out.println("dr "
			 * +delta[i].getRows()); System.out.println(activation[i -
			 * 1].getColumns()); System.out.println(activation[i -
			 * 1].getRows()); System.out.println("FOO");
			 */
			doShit(f, delta[i], activation[i - 1]);
			/*
			 * f.addiColumnVector(delta[i]); f.muliRowVector(activation[i - 1]);
			 */
			derative[i - 1] = f;
		}

		for (int i = biasWeights.length; i < derative.length; i++) {
			derative[i] = delta[1 + i - weights.length];
		}
		return derative;
	}

}
