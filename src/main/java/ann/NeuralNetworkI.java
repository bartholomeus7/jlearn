package ann;

import java.io.Serializable;

import org.jblas.DoubleMatrix;

import data.DataSet;

public interface NeuralNetworkI extends Savable<NeuralNetworkI>, Serializable {

	public void train(DataSet<?> data, boolean parallel);

	public int getIntputSize();

	public int getOutputSize();

	public DoubleMatrix[] getDerative(DataSet<?> data, boolean parallel);

	public void setTransferFunction(C1Function f, int layer);

	public void setTransferFunction(C1Function f);

	public DoubleMatrix[] getNullGradient();

	public void setTrainer(AnnTrainer trainer);

	public AnnTrainer getTrainer();

	public void update(DoubleMatrix update, int idx);

	public int layers();

	public double getError(DataSet<?> data);

	public double[] calculate(double[] input);

}
