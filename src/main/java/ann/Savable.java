package ann;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public interface Savable<X> extends Serializable {
	public default void saveToFile(String file) {
		try (FileOutputStream fout = new FileOutputStream(file);
				ObjectOutputStream oos = new ObjectOutputStream(fout);) {
			oos.writeObject(this);
		} catch (Exception ex) {
			throw new RuntimeException("Error on saving multilayer perceptron.", ex);
		}
	}

	@SuppressWarnings("unchecked")
	public default X loadFromFile(String file) {
		try (FileInputStream fin = new FileInputStream(file); ObjectInputStream ois = new ObjectInputStream(fin);) {
			return (X) ois.readObject();
		} catch (Exception ex) {
			throw new RuntimeException("Error on loading multilayer perceptron.", ex);
		}
	}
}
