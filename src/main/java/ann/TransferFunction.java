package ann;

import java.io.Serializable;

public enum TransferFunction implements C1Function,Serializable {
	SIGMOID {

		@Override
		public double func(double x) {
			return 0.5d * (1 + Math.tanh(x * 0.5d));
		}

		@Override
		public double derative(double x) {
			double sig = func(x);
			return sig * (1 - sig);
		}

	},
	LINEAR {

		@Override
		public double func(double x) {
			return x;
		}

		@Override
		public double derative(double x) {
			return 1;
		}

	},
	TANH {

		@Override
		public double func(double x) {
			return Math.tanh(x);
		}

		@Override
		public double derative(double x) {
			double temp = func(x);
			return 1 - temp * temp;
		}

	},

	CONTINUOUS_RECTIFIER {

		@Override
		public double func(double x) {
			return Math.log(1 + Math.exp(x));
		}

		@Override
		public double derative(double x) {
			return 1 / (1 + Math.exp(-x));
		}
	},

	RECTIFIED_LINEAR_UNIT {

		@Override
		public double func(double x) {
			return Math.max(0, x);
		}

		@Override
		public double derative(double x) {
			if (x > 0)
				return 1d;
			else
				return 0d;
		}
	},

	BOUNDED_LINEAR {

		@Override
		public double func(double x) {
			if (Math.abs(x) <= 1)
				return x;
			else
				return Math.signum(x);
		}

		@Override
		public double derative(double x) {
			if (Math.abs(x) >= 1)
				return 0;
			else
				return 1;
		}

	}

}
