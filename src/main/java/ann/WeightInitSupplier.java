package ann;

import java.util.function.Supplier;

public interface WeightInitSupplier {
	public Supplier<Double> getWeightInit(int layerFrom, int layerTo);
}
