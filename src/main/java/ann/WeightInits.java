package ann;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public enum WeightInits implements WeightInitSupplier {
	GAUSSIAN {
		@Override
		public Supplier<Double> getWeightInit(int layerFrom, int layerTo) {
			Random r = new Random();
			return () -> r.nextGaussian() * 0.1d;
		}
	},

	SHUFFLE_EVEN {

		@Override
		public Supplier<Double> getWeightInit(int layerFrom, int layerTo) {
			int max = layerFrom * layerTo;
			List<Double> result = IntStream.range(0, layerFrom * layerTo).mapToDouble(i -> -1d + 2d * max / i).boxed()
					.collect(Collectors.toList());
			Collections.shuffle(result);
			return () -> result.remove(result.size() - 1);
		}
	},

	NULL {

		@Override
		public Supplier<Double> getWeightInit(int layerFrom, int layerTo) {
			return () -> 0d;
		}

	},

	ABS_GAUSSIAN {
		@Override
		public Supplier<Double> getWeightInit(int layerFrom, int layerTo) {
			Random r = new Random();
			return () -> Math.abs(r.nextGaussian());
		}
	}

	;

}
