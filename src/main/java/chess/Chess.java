package chess;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import chess.Chess.ChessBoard;
import chess.board.AtkDefBoard;
import chess.board.Board;
import chess.board.ChessMove;
import chess.mgen.AllMoveGenerator;
import chess.mgen.MoveGenerator;
import chess.misc.IntList;
import game.Game;
import game.GameBoard;

public class Chess implements Game<ChessMove, ChessBoard> {

	private static Chess instance;

	private Chess() {

	}

	@Override
	public ChessBoard buildStartingBoard() {
		return new ChessBoard();
	}

	public static class ChessBoard implements GameBoard<ChessMove, ChessBoard> {
		private final Board b;
		private final Stack<AtkDefBoard> atkdefmaps = new Stack<>();
		private MoveGenerator gen = new AllMoveGenerator();

		public ChessBoard() {
			this(new Board());
		}

		private ChessBoard(ChessBoard other) {
			this(other.getBoard());
		}

		public ChessBoard(Board board) {
			this.b = new Board(board);
			atkdefmaps.push(new AtkDefBoard(b));
		}

		@Override
		public List<ChessMove> getMoves() {
			IntList moveList = gen.generateMoves(b, new IntList());
			List<ChessMove> result = new ArrayList<ChessMove>(moveList.size());
			moveList.forEach(i -> result.add(new ChessMove(i)));
			return result;
		}

		public Board getBoard() {
			return b;
		}

		@Override
		public boolean isFinished() {
			return b.getGameState().result.ended;
		}

		@Override
		public void pushMove(ChessMove m) {
			b.pushMove(m.toInt());
			atkdefmaps.push(new AtkDefBoard(b));
		}

		@Override
		public void popMove(ChessMove m) {
			b.popMove(m.toInt());
			atkdefmaps.pop();
		}

		public AtkDefBoard getAtkDefInfos() {
			return atkdefmaps.peek();
		}

		@Override
		public ChessBoard copy() {
			return new ChessBoard(this);
		}

		@Override
		public String toString() {
			return b.toString();
		}

		@Override
		public long longhash() {
			return b.getZobrist();
		}

		@Override
		public int getPlayerToMove() {
			return b.getCurrentColor();
		}

		@Override
		public game.GameBoard.GameState getGameState() {
			switch (b.getGameState().result) {
			case WIN:
				return b.isWhiteToMove() ? GameState.PLAYER2_WON : GameState.PLAYER1_WON;
			case DRAW:
				return GameState.DRAW;
			default:
				return GameState.NO_END;
			}
		}

		@Override
		public boolean isLegalMove(ChessMove move) {
			return this.getMoves().contains(move);
		}

		@Override
		public void print() {
			b.printBoard();
		}

		@Override
		public List<ChessMove> getVariation() {
			return b.getVariation();
		}

		@Override
		public boolean isReversible() {
			return b.hasReversibleMove();
		}

		@Override
		public boolean isRepetition() {
			return b.isRepetition();
		}

		@Override
		public Game<ChessMove, ChessBoard> getGame() {
			return Chess.getInstance();
		}

	}

	@Override
	public ChessBoard buildBoardFromString(String string) {
		return new ChessBoard(new Board(string));
	}

	public static Chess getInstance() {
		if (instance == null)
			instance = new Chess();
		return instance;
	}

}
