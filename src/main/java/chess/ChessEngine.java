package chess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import FourInARow.FourInARow.FourBoard;
import ann.MultiLayerPerceptron;
import ann.NeuralNetworkI;
import chess.Chess.ChessBoard;
import chess.board.Board;
import chess.board.ChessMove;
import chess.board.EPDBoard;
import dis.RemoteVirtualMachine;
import game.GameBoard.GameState;
import ml.FeatureFunction;
import ml.NeuronalEval;
import search.ABEngine;
import search.AspirationWindow;
import search.KillerMoveHeuristic;
import search.Search;

public class ChessEngine {

	private static final Logger logger = Logger.getLogger(ChessEngine.class);

	public static void main(String[] args) {

		ABEngine<ChessMove, ChessBoard> first = getEngine(true, "best315.net");
		/*
		 * BiFunction<String, ChessBoard, ChessMove> moveParser = (s, b) -> new
		 * ChessMove(ChessStrings.parseSanMove(s, b.getBoard()));
		 * Player<ChessMove, ChessBoard> player = Player.getRandomPlayer();
		 * Player<ChessMove, ChessBoard> player2 = Player.getEnginePlayer(first,
		 * 2000);
		 * 
		 * Match<?, ?> m = new Match<ChessMove, ChessBoard>(player2, player, new
		 * ChessBoard()); m.playUntilEnd();
		 */

		// testSearch(first, 1000);
		// testPosition(second, new ChessBoard(b));
		// testSelfPlay(first, 1000);

		// testDual(first, second, 1000);

		strategicTest(first);

	}

	private static ABEngine<Integer, FourBoard> getFIAREngine(String netName) {
		NeuralNetworkI net = (MultiLayerPerceptron) new MultiLayerPerceptron(1).loadFromFile(netName);
		NeuronalEval<Integer, FourBoard> eval = new NeuronalEval<Integer, FourBoard>(net,
				new FeatureFunction<FourBoard>() {

					@Override
					public double[] apply(FourBoard t) {
						return t.getFeature();
					}

					@Override
					public int features() {
						return 42;
					}
				});
		ABEngine<Integer, FourBoard> engine = new ABEngine<>(eval);
		engine.addPlugin(new KillerMoveHeuristic<>());
		// engine.addPlugin(new Statistic<>());
		return engine;
	}

	private static ABEngine<ChessMove, ChessBoard> getEngine(boolean nullMove, String netName) {
		ChessFeature feature = ChessFeature.getDefaultFeature();

		NeuralNetworkI net = (MultiLayerPerceptron) new MultiLayerPerceptron(1).loadFromFile(netName);
		NeuronalEval<ChessMove, ChessBoard> eval = new NeuronalEval<>(net, feature);
		ChessEval3 eval2 = new ChessEval3(new Board());
		ABEngine<ChessMove, ChessBoard> engine = new ABEngine<>(eval2);
		engine.addPlugin(ChessHeuristics.getQsceSearch());
		engine.addPlugin(new AspirationWindow<>());
		engine.addPlugin(ChessHeuristics.getChessFutilityPruning(3d / 100d));
		engine.addPlugin(new GoodCaptureHeuristic());
		if (nullMove)
			engine.addPlugin(ChessHeuristics.getNullMoveHeuristic());
		engine.addPlugin(new KillerMoveHeuristic<>());
		engine.addPlugin(new RemainingCaptureHeuristic());
		engine.addPlugin(new HistoryHeuristic());
		// engine.addPlugin(new Statistic<>());
		return engine;
	}

	private static ABEngine<ChessMove, ChessBoard> getEngine(boolean nullMove) {
		return getEngine(nullMove, "spirit37.net");
	}

	private static double testDual(ABEngine<ChessMove, ChessBoard> first, ABEngine<ChessMove, ChessBoard> second,
			int games) {
		int total = 0;
		double player1Score = 0;
		for (ChessBoard b : ChessLearner.chessIterator()) {
			GameState g1 = testPlay(first, second, b.copy());
			System.out.println(g1);
			player1Score += g1.getPlayer1Score();
			GameState g2 = testPlay(second, first, b.copy());
			System.out.println(g2);
			player1Score += g2.getPlayer2Score();
			total++;
			logger.info("Player1 score : " + player1Score + "/" + total * 2);
			if (total > games)
				break;

		}
		return player1Score;
	}

	private static void testSearch(ABEngine<ChessMove, ChessBoard> engine, int games) {
		int total = 0;
		for (ChessBoard b : ChessLearner.chessIterator()) {
			Search<?, ?> seach = engine.buildSearch(b, 8);
			seach.setMaxSearchTime(1l, TimeUnit.SECONDS);
			seach.run();
			total++;
			if (total > games)
				break;

		}
	}

	private static GameState testPlay(ABEngine<ChessMove, ChessBoard> engine, ABEngine<ChessMove, ChessBoard> second,
			ChessBoard board) {
		while (!board.isFinished()) {
			ABEngine<ChessMove, ChessBoard> current = board.getPlayerToMove() == 0 ? engine : second;
			Search<ChessMove, ChessBoard> s = current.buildSearch(board, 10);
			s.setMaxSearchTime(10l, TimeUnit.MILLISECONDS);
			s.run();
			board.pushMove(s.getBestMove());
			if (board.isFinished()) {
				System.out.println(s.getEval());

				return board.getGameState();
			}
		}
		throw new IllegalStateException("Game never ended");
	}

	private static void testSelfPlay(ABEngine<ChessMove, ChessBoard> engine, int milisPerMove) {
		ChessBoard board = new ChessBoard();
		List<Search<?, ?>> searches = new ArrayList<>();
		while (!board.isFinished()) {
			System.out.println("Static eval = " + engine.staticEval(board));
			Search<ChessMove, ChessBoard> s = engine.buildSearch(board, 10);
			s.setMaxSearchTime(milisPerMove, TimeUnit.MILLISECONDS);
			s.logSearch(Logger.getLogger(Chess.class), Priority.INFO);
			s.run();
			searches.add(s);

			board.pushMove(s.getBestMove());
			board.print();
			if (board.isFinished()) {

				System.out.println(board.getBoard().isStaleMate());
				System.out.println(board.getBoard().is3FoldRepetition());
				System.out.println(board.getBoard().isCheckmate());
			}
			System.out.println(board.getGameState());
			System.out.println(board.isFinished());
			System.out.println(board.getBoard().getGameState());
			// System.out.println(board.getMoves());
			System.out.println(board.getBoard().isCheckmate());

			if (board.isFinished() && board.getBoard().is3FoldRepetition()) {
				board.popMove(s.getBestMove());
				board.print();
				s = engine.buildSearch(board, 10);
				// s.setMaxSearchTime(milisPerMove, TimeUnit.MILLISECONDS);
				s.logSearch(Logger.getLogger(Chess.class), Priority.INFO);
				s.run();
			}
			engine.clear();

		}
	}

	private static void testPosition(ABEngine<ChessMove, ChessBoard> engine, ChessBoard board) {

		Search<ChessMove, ChessBoard> s = engine.buildSearch(board, 10);
		s.setMaxSearchTime(15l, TimeUnit.SECONDS);
		s.logSearch(Logger.getLogger(Chess.class), Priority.INFO);
		s.run();
		System.out.println("PV=" + s.getPricipalVariation());
		board.pushMove(s.getBestMove());
		board.print();
		if (board.isFinished()) {
			System.out.println(board.getBoard().isStaleMate());
			System.out.println(board.getBoard().is3FoldRepetition());
			System.out.println(board.getBoard().isCheckmate());
		}

	}

	private static void strategicTest(ABEngine<ChessMove, ChessBoard> engine) {
		List<EPDBoard> boards = EPDBoard
				.getEPDBoards("/home/fortknock/workspace (2)/FooChess/test/ressources/strategictest");
		int sum = 0;
		int count = 0;
		Map<String, Integer> scoreMap = new HashMap<>();
		Map<String, Integer> maxMap = new HashMap<>();
		Pattern ID_PATTERN = Pattern.compile("(\"STS\\(.*\\)){0,1}(.*)\\..*");

		for (EPDBoard test : boards) {
			String id = test.getOperations().get("id");
			logger.info("Testing " + id + " ...");
			Matcher m = ID_PATTERN.matcher(id);
			m.find();
			String testGroup = m.group(2);
			System.out.println(testGroup);

			Search<ChessMove, ChessBoard> s = engine.buildSearch(new ChessBoard(new Board(test.getBoard().getFen())),
					10);
			s.setMaxSearchTime(100, TimeUnit.MILLISECONDS);
			// s.logSearch(Logger.getLogger(Chess.class), Priority.INFO);
			s.run();

			engine.clear();
			int score = test.scoreMove(s.getBestMove());

			scoreMap.compute(testGroup, (k, v) -> v == null ? score : v + score);
			maxMap.compute(testGroup, (k, v) -> v == null ? 10 : v + 10);

			logger.info("Score  :" + score);
			sum += score;
			logger.info("Summed Score : " + sum + " / " + count++ * 10);
		}
		logger.info("Test per test group : ");
		for (String key : scoreMap.keySet()) {
			logger.info("Score in " + key + ":" + scoreMap.get(key) + "/" + maxMap.get(key));
		}
	}

	private static void updateNet() {
		try (RemoteVirtualMachine jvm = new RemoteVirtualMachine("fortknock", "192.168.1.2")) {
			// jvm.download("spirit38.net");
			jvm.download("spirit38.net");
			jvm.download("best315.net");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
