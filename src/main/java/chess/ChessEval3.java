package chess;

import java.util.function.ToDoubleFunction;

import chess.Chess.ChessBoard;
import chess.board.BB;
import chess.board.Board;
import chess.board.ChessMove;
import chess.misc.Bits;

public class ChessEval3 implements ToDoubleFunction<ChessBoard> {
	static final int bishopEval = 325, knightEval = 275, queenEval = 875, rookEval = 475, pawnEval = 120, kingEval = 0;

	private static final int[] bishopEvals = initEquiDistantArray(bishopEval, 100, 256);
	private static final int[] knightEvals = initEquiDistantArray(knightEval, 100, 256);
	private static final int[] queenEvals = initEquiDistantArray(queenEval, 200, 256);
	private static final int[] rookEvals = initEquiDistantArray(rookEval, 100, 256);
	private static final int[] kingEvals = initEquiDistantArray(kingEval, 250, 256);
	private static final int[] pawnEvals = initEquiDistantArray(pawnEval, 50, 256);
	private static final int[][] pieceEvals = initPieceEvals();

	private static long bbInnerCenter = 0x18l << 24 | 0x18l << 32;
	private static long bbMiddleCenter = initMiddleCenter();
	private static long bb36rank = BB.RANK4OR5 | BB.RANKS[16] | BB.RANKS[40];

	private int eval = 0;
	private int evalOpening = 0;
	private final long white, black, occ, wPawnWeakness, bPawnWeakness, whiteAttacks, blackAttacks, nonWeakWhite,
			nonWeakBlack;
	private long whiteHoles, blackHoles;
	private final int whiteX;
	private final int blackX;

	private static int[] color = new int[] { 1, -1 };

	private static final int[] knightPawn = new int[] { -10, -10, -10, -10, -8, -6, -4, -2, -0, 2, 4, 6, 8, 10, 10, 10,
			10 };
	private static final int[] rookPawn = new int[] { 20, 20, 20, 20, 15, 12, 9, 6, 3, 0, -3, -8, -13, -18, -23, -23,
			-23 };

	private static final int[] undefended = new int[] { -10, +10, -26, +26, -32, +32, -45, +45, -72, 72, -30, +30 };

	private static final int[] kingPwnTrop = initEquiDistantArray(-20, 80, 22);

	private static final int[] pawnPerLine = new int[] { 0, 5, 10, 15, 15, 10, 5, 0 };
	private static final int[] passedPawn = new int[] { 0, 5, 10, 20, 30, 45, 60, 75 };
	private static final int[] pwnConnect = new int[] { -45, -5, 0, 5, 10 };

	private static final long[] whiteTrop = initWhiteTrop();
	private static final long[] blackTrop = initBlackTrop();
	private static final int[] tropLup = initTropLup();

	private static int[] PAWN_PIECE_TABLE = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 50, 50, 50, 50, 50, 50, 50, 50, 10, 10,
			20, 30, 30, 20, 10, 10, 5, 5, 10, 25, 25, 10, 5, 5, 0, 0, 0, 20, 20, 0, 0, 0, 5, -5, -10, 0, 0, -10, -5, 5,
			5, 10, 10, -20, -20, 10, 10, 5, 0, 0, 0, 0, 0, 0, 0, 0 };

	private static int[] KNIGHT_PIECE_TABLE = new int[] { -50, -40, -30, -30, -30, -30, -40, -50, -40, -20, 0, 0, 0, 0,
			-20, -40, -30, 0, 10, 15, 15, 10, 0, -30, -30, 5, 15, 20, 20, 15, 5, -30, -30, 0, 15, 20, 20, 15, 0, -30,
			-30, 5, 10, 15, 15, 10, 5, -30, -40, -20, 0, 5, 5, 0, -20, -40, -50, -40, -30, -30, -30, -30, -40, -50, };

	private static int[] BISHOP_PIECE_TABLE = new int[] { -20, -10, -10, -10, -10, -10, -10, -20, -10, 0, 0, 0, 0, 0, 0,
			-10, -10, 0, 5, 10, 10, 5, 0, -10, -10, 5, 5, 10, 10, 5, 5, -10, -10, 0, 10, 10, 10, 10, 0, -10, -10, 10,
			10, 10, 10, 10, 10, -10, -10, 5, 0, 0, 0, 0, 5, -10, -20, -10, -10, -10, -10, -10, -10, -20, };

	private static int[] ROOK_PIECE_TABLE = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 5, 10, 10, 10, 10, 10, 10, 5, -5, 0, 0,
			0, 0, 0, 0, -5, -5, 0, 0, 0, 0, 0, 0, -5, -5, 0, 0, 0, 0, 0, 0, -5, -5, 0, 0, 0, 0, 0, 0, -5, -5, 0, 0, 0,
			0, 0, 0, -5, 0, 0, 0, 5, 5, 0, 0, 0 };

	private static int[] QUEEN_PIECE_TABLE = new int[] { -20, -10, -10, -5, -5, -10, -10, -20, -10, 0, 0, 0, 0, 0, 0,
			-10, -10, 0, 5, 5, 5, 5, 0, -10, -5, 0, 5, 5, 5, 5, 0, -5, 0, 0, 5, 5, 5, 5, 0, -5, -10, 5, 5, 5, 5, 5, 0,
			-10, -10, 0, 5, 0, 0, 0, 0, -10, -20, -10, -10, -5, -5, -10, -10, -20 };

	private static int[] KING_MIDDLE_GAME = new int[] { -20, -10, -10, -5, -5, -10, -10, -20, -10, 0, 0, 0, 0, 0, 0,
			-10, -10, 0, 5, 5, 5, 5, 0, -10, -5, 0, 5, 5, 5, 5, 0, -5, 0, 0, 5, 5, 5, 5, 0, -5, -10, 5, 5, 5, 5, 5, 0,
			-10, -10, 0, 5, 0, 0, 0, 0, -10, -20, -10, -10, -5, -5, -10, -10, -20 };

	private static int[] KING_ENDGAME = new int[] { -50, -40, -30, -20, -20, -30, -40, -50, -30, -20, -10, 0, 0, -10,
			-20, -30, -30, -10, 20, 30, 30, 20, -10, -30, -30, -10, 30, 40, 40, 30, -10, -30, -30, -10, 30, 40, 40, 30,
			-10, -30, -30, -10, 20, 30, 30, 20, -10, -30, -30, -30, 0, 0, 0, 0, -30, -30, -50, -30, -30, -30, -30, -30,
			-30, -50 };

	private static int[][] PIECE_TABLES = new int[][] { PAWN_PIECE_TABLE, negative_reverse(PAWN_PIECE_TABLE),
			KNIGHT_PIECE_TABLE, negative_reverse(KNIGHT_PIECE_TABLE), BISHOP_PIECE_TABLE,
			negative_reverse(BISHOP_PIECE_TABLE), ROOK_PIECE_TABLE, negative_reverse(ROOK_PIECE_TABLE),
			QUEEN_PIECE_TABLE, negative_reverse(QUEEN_PIECE_TABLE) };

	private static int[] negative_reverse(int[] array) {
		int[] result = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			result[array.length - i - 1] = -array[i];
		}
		return result;
	}

	private static int[] initEquiDistantArray(int center, int range, int entries) {
		int[] result = new int[entries];
		float stepSize = (float) (2 * range) / (float) entries;
		int start = center - range;
		for (int i = 0; i < entries; i++) {
			result[i] = Math.round(start + (i + 1) * stepSize);
		}
		return result;
	}

	private static int[] initTropLup() {
		int[] result = new int[16 * 64];
		int[] openLineMalus = new int[] { -40, -35, -35, -60, -60, -35, -35, -40 };
		int[] shieldLup = new int[] { -40, 0, +25, +35 };

		for (int i = 0; i < result.length; i++) {
			int color = i >>> 9;
			int line = (i >>> 6) & 7;
			int shield = i & 0x3f;
			// int eval = lineEval[line];
			int eval = 0;
			int directShield = shield & 7;
			if (color == 1) {
				// if black, simply mirrow stuff
				shield = Integer.reverse(shield) >>> 26;
				result[i] = result[line << 6 | shield];
				continue;
			}

			if ((shield & 18) == 0) {
				// System.out.println("open");
				eval += openLineMalus[line];
				if (line < 3 || line > 4) {

					int toCenter = line < 3 ? 36 : 9;
					toCenter &= shield;
					if (Long.bitCount(toCenter) == 2) {
						eval -= 20;
					}
				} else {

				}

			} else {
				eval += shieldLup[Integer.bitCount(directShield)];
			}

			eval += Long.bitCount(shield) * 2;
			// System.out.println(eval);
			result[i] = eval;
		}
		return result;
	}

	private static long[] initWhiteTrop() {
		long[] result = new long[64];
		for (int i = 55; i > 0; i--) {
			int top = i + 8;
			result[i] = BB.kingAttacks(i) & BB.RANKS[top];
			result[i] |= result[top] & BB.RANKS[(top + 8) % 64];
		}
		return result;
	}

	private static long[] initBlackTrop() {
		long[] result = new long[64];
		for (int i = 8; i < 64; i++) {
			int bottom = i - 8;
			result[i] = BB.kingAttacks(i) & BB.RANKS[bottom];
			result[i] |= result[bottom] & BB.RANKS[(64 + bottom - 8) % 64];
		}
		return result;
	}

	private static int[] negativeCopy(int[] array) {
		int[] copy = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			copy[i] = -array[i];
		}
		return copy;
	}

	private static int[][] initPieceEvals() {
		int[][] result = new int[12][];
		result[Board.WHITE_PAWN] = pawnEvals;
		result[Board.WHITE_KNIGHT] = knightEvals;
		result[Board.WHITE_ROOK] = rookEvals;
		result[Board.WHITE_QUEEN] = queenEvals;
		result[Board.WHITE_BISHOP] = bishopEvals;
		result[Board.WHITE_KING] = kingEvals;

		result[Board.BLACK_PAWN] = negativeCopy(pawnEvals);
		result[Board.BLACK_KNIGHT] = negativeCopy(knightEvals);
		result[Board.BLACK_ROOK] = negativeCopy(rookEvals);
		result[Board.BLACK_QUEEN] = negativeCopy(queenEvals);
		result[Board.BLACK_BISHOP] = negativeCopy(bishopEvals);

		result[Board.BLACK_KING] = negativeCopy(kingEvals);

		return result;
	}

	private static long initMiddleCenter() {
		long rank3456 = 0, file3456 = 0l;
		for (int i = 2; i <= 5; i++)
			rank3456 |= BB.RANKS[8 * i];
		for (int i = 2; i <= 5; i++) {
			file3456 |= BB.FILES[i];
		}
		return rank3456 & file3456;
	}

	private int kingPawnTrop(int king) {
		int manh = 0;
		if (whiteX + blackX == 0)
			return 0;
		long passed = (white & blackHoles) | (black & whiteHoles);
		long it = passed;
		while (it != 0) {
			int sq = Bits.bitscanForward(it);
			it &= it - 1;
			manh += BB.manhatten(king, sq);
		}
		manh *= 3;
		long pawns = occ & ~passed;
		while (pawns != 0) {
			int sq = Bits.bitscanForward(pawns);
			pawns &= pawns - 1;
			manh += BB.manhatten(king, sq);
		}
		manh /= whiteX + blackX;

		return ChessEval3.kingPwnTrop[manh];
	}

	public ChessEval3(Board b) {
		this.white = b.getBitboard(Board.WHITE_PAWN);
		this.black = b.getBitboard(Board.BLACK_PAWN);
		occ = white | black;
		whiteAttacks = BB.pawnAttacks(white, 0);
		blackAttacks = BB.pawnAttacks(black, 1);
		nonWeakWhite = (BB.eastOne(white) | BB.westOne(white) | whiteAttacks) & white;
		nonWeakBlack = (BB.eastOne(black) | BB.westOne(black) | blackAttacks) & black;
		wPawnWeakness = white & ~nonWeakWhite;
		bPawnWeakness = black & ~nonWeakBlack;
		blackHoles = BB.southFill(blackAttacks);
		blackHoles = ~blackHoles;
		whiteHoles = BB.nortFill(whiteAttacks);
		whiteHoles = ~whiteHoles;
		whiteX = Long.bitCount(white);
		blackX = Long.bitCount(black);
		evalPawnStructure();
	}

	public void evalPawnStructure() {
		long bb = white;

		while (bb != 0) {

			int idx = Bits.bitscanForward(bb);
			// System.out.println(idx);
			bb &= bb - 1;
			this.evalOpening += this.pieceEvalOpening(Board.WHITE_PAWN, idx);
		}

		bb = black;

		while (bb != 0) {

			int idx = Bits.bitscanForward(bb);
			bb &= bb - 1;
			this.evalOpening += this.pieceEvalOpening(Board.BLACK_PAWN, idx);

		}

		int spaceN = 0;
		long space = BB.southFill(white);
		spaceN += Long.bitCount(space) * 4;
		space = BB.nortFill(black);
		spaceN -= 4 * Long.bitCount(space);
		this.evalOpening += spaceN;

		eval = evalOpening;
	}

	private int pieceEval(int sq, int piece, int phase) {
		// System.out.println(piece);
		/**
		 * eval += (24 - phase) (pieceEvalEndgame(piece, sq) +
		 * ptPieceE[piece][sq]); return eval;
		 */
		int eval = (24 - phase) * pieceEvalOpening(piece, sq);
		eval += (phase) * (pieceEvalEndgame(piece, sq));

		int eval2 = 0;
		if (piece < Board.WHITE_KING) {
			eval2 += PIECE_TABLES[piece][sq];
		} else {
			eval2 += ((24 - phase) * KING_MIDDLE_GAME[sq] + phase * KING_ENDGAME[sq]) / 24;
		}
		if ((piece & 1) == 0) {
			eval += eval2 * 24;
		} else {
			eval -= eval2 * 24;
		}

		return eval;
	}

	public int evalCapture(int move, Board b) {
		int to = ChessMove.getTo(move);
		int from = ChessMove.getFrom(move);
		int piece = b.getPiece(from);
		int capt = b.getPiece(to);
		return color[piece & 1]
				* (pieceEvalOpening(piece, to) - pieceEvalOpening(piece, from) + pieceEvalOpening(capt, to));
	}

	public int pieceEvalOpening(int piece, int sq) {
		return pieceEvals[piece][evalPieceOpening(piece, sq)];
	}

	public int pieceEvalEndgame(int piece, int sq) {
		return pieceEvals[piece][evalPieceEndgame(piece, sq)];
	}

	private int evalPieceEndgame(int piece, int sq) {
		long attacks;
		int eval;
		switch (piece) {

		case Board.WHITE_BISHOP:
			attacks = BB.bishopAttacks(sq, occ);
			return 125 + 4 * Bits.sparsePopCount(attacks & bPawnWeakness) + 2 * (Long.bitCount(attacks & ~white))
					+ 5 * Bits.sparsePopCount(attacks & wPawnWeakness) - Long.bitCount(BB.getColor(sq) & white);
		case Board.BLACK_BISHOP:
			attacks = BB.bishopAttacks(sq, occ);
			return 125 + 4 * Bits.sparsePopCount(attacks & bPawnWeakness) + 2 * (Long.bitCount(attacks & ~black))
					+ 5 * Bits.sparsePopCount(attacks & bPawnWeakness) - Long.bitCount(BB.getColor(sq) & white);
		case Board.WHITE_KNIGHT:
			attacks = BB.KnightAttacks(sq);
			return 84

					+ 3 * (Long.bitCount(attacks)) + 3 * Bits.sparsePopCount(attacks & wPawnWeakness)
					+ 5 * Bits.sparsePopCount(attacks & bPawnWeakness)
					+ (1 & (int) ((1l << sq & blackHoles & bb36rank & whiteAttacks) - 1) >> 63) * 20
					+ knightPawn[blackX + whiteX];
		case Board.BLACK_KNIGHT:
			attacks = BB.KnightAttacks(sq);
			return 84 + 3 * (Long.bitCount(attacks)) + 3 * Bits.sparsePopCount(attacks & bPawnWeakness)
					+ 5 * Bits.sparsePopCount(attacks & wPawnWeakness)
					+ (1 & (int) ((1l << sq & whiteHoles & bb36rank & blackAttacks) - 1) >> 63) * 20
					+ knightPawn[blackX + whiteX];
		case Board.WHITE_ROOK:
			attacks = BB.rookAttacks(sq, occ);
			return 80 + 2 * (Long.bitCount(attacks & BB.FILES[sq])) + 2 * (Long.bitCount(attacks & BB.RANKS[sq]))
					+ 4 * Bits.sparsePopCount(attacks & wPawnWeakness)
					+ 8 * Bits.sparsePopCount(attacks & bPawnWeakness) - 6 * Bits.sparsePopCount(attacks & nonWeakWhite)
					+ rookPawn[whiteX + blackX];
		case Board.BLACK_ROOK:
			attacks = BB.rookAttacks(sq, occ);
			return 80 + 2 * (Long.bitCount(attacks & BB.FILES[sq])) + 2 * (Long.bitCount(attacks & BB.RANKS[sq]))
					+ 4 * Bits.sparsePopCount(attacks & bPawnWeakness)
					+ 8 * Bits.sparsePopCount(attacks & wPawnWeakness) - 6 * Bits.sparsePopCount(attacks & nonWeakBlack)
					+ rookPawn[whiteX + blackX];
		case Board.WHITE_PAWN:
			eval = 80;
			attacks = BB.PAWN_ATTACKS[0][sq];
			eval += pawnPerLine[sq & 7];
			eval += pwnConnect[Long.bitCount((BB.pawnConnect(0, sq) & white))];
			eval += +(1 & (int) ((attacks & bbInnerCenter) - 1) >> 63) * 10;
			eval += +3 * Bits.sparsePopCount(attacks & bbMiddleCenter);
			eval += 1 & ((int) ((((1l << sq) & blackHoles)) - 1) >> 63) * passedPawn[sq >>> 3];
			eval += 20 * Bits.sparsePopCount(BB.pawnConnect(0, sq) & white & blackHoles);
			return eval;
		case Board.BLACK_PAWN:
			eval = 80;
			attacks = BB.PAWN_ATTACKS[1][sq];
			eval += pawnPerLine[sq & 7];
			eval += pwnConnect[Long.bitCount((BB.pawnConnect(0, sq) & black))];
			eval += +(1 & (int) ((1l << sq & bbInnerCenter) - 1) >> 63) * 10;
			eval += +3 * Bits.sparsePopCount(attacks & bbMiddleCenter);
			eval += 1 & ((int) ((((1l << sq) & whiteHoles)) - 1) >> 63) * passedPawn[7 - (sq >>> 3)];
			eval += 20 * Bits.sparsePopCount(BB.pawnConnect(0, sq) & black & whiteHoles);
			return eval;
		case Board.WHITE_KING:
			return 128 + this.kingPawnTrop(sq);
		case Board.BLACK_KING:
			return 128 + this.kingPawnTrop(sq);
		case Board.WHITE_QUEEN:
			attacks = BB.queenAttacks(sq, occ);
			return (80 + 3 * Long.bitCount(attacks));
		case Board.BLACK_QUEEN:
			attacks = BB.queenAttacks(sq, occ);
			return (80 + 3 * Long.bitCount(attacks));
		default:
			return 0;
		}
	}

	private int evalPieceOpening(int piece, int sq) {
		long attacks;
		int eval;
		long shield;
		switch (piece) {

		case Board.WHITE_BISHOP:
			attacks = BB.bishopAttacks(sq, occ);
			return 125 + 8 * Bits.sparsePopCount(attacks & bPawnWeakness) + 2 * (Long.bitCount(attacks & ~white))
					+ 4 * (Bits.sparsePopCount(attacks & bbInnerCenter))
					+ 5 * Bits.sparsePopCount(attacks & wPawnWeakness) - 2 * Long.bitCount(BB.getColor(sq) & white);
		case Board.BLACK_BISHOP:
			attacks = BB.bishopAttacks(sq, occ);
			return 125 + 8 * Bits.sparsePopCount(attacks & wPawnWeakness) + 2 * (Long.bitCount(attacks & ~black))
					+ 4 * (Bits.sparsePopCount(attacks & bbInnerCenter))
					+ 5 * Bits.sparsePopCount(attacks & bPawnWeakness) - 2 * Long.bitCount(BB.getColor(sq) & black);
		case Board.WHITE_KNIGHT:
			attacks = BB.KnightAttacks(sq);
			return 80 + 4 * Bits.sparsePopCount(attacks & bbInnerCenter)
					+ 2 * Bits.sparsePopCount(attacks & bbMiddleCenter) + 3 * (Long.bitCount(attacks))
					+ 3 * Bits.sparsePopCount(attacks & wPawnWeakness)
					+ 5 * Bits.sparsePopCount(attacks & bPawnWeakness)
					+ (1 & (int) (((1l << sq) & blackHoles & bb36rank & whiteAttacks) - 1) >> 63) * 35
					+ knightPawn[blackX + whiteX];
		case Board.BLACK_KNIGHT:
			attacks = BB.KnightAttacks(sq);
			return 80 + 4 * Bits.sparsePopCount(attacks & bbInnerCenter)
					+ 2 * Bits.sparsePopCount(attacks & bbMiddleCenter) + 3 * (Long.bitCount(attacks))
					+ 3 * Bits.sparsePopCount(attacks & bPawnWeakness)
					+ 5 * Bits.sparsePopCount(attacks & wPawnWeakness)
					+ (1 & (int) (((1l << sq) & whiteHoles & bb36rank & blackAttacks) - 1) >> 63) * 35
					+ knightPawn[blackX + whiteX];
		case Board.WHITE_ROOK:
			attacks = BB.rookAttacks(sq, occ);
			return 60 + 4 * (Long.bitCount(attacks & BB.FILES[sq])) + 2 * (Long.bitCount(attacks & BB.RANKS[sq]))
					+ 4 * Bits.sparsePopCount(attacks & bPawnWeakness)
					- 2 * Bits.sparsePopCount(attacks & nonWeakWhite);
		case Board.BLACK_ROOK:
			attacks = BB.rookAttacks(sq, occ);
			return 60 + 4 * (Long.bitCount(attacks & BB.FILES[sq])) + 2 * (Long.bitCount(attacks & BB.RANKS[sq]))
					+ 4 * Bits.sparsePopCount(attacks & wPawnWeakness)
					- 2 * Bits.sparsePopCount(attacks & nonWeakBlack);
		case Board.WHITE_PAWN:
			eval = 80;
			attacks = BB.PAWN_ATTACKS[0][sq];
			eval += pawnPerLine[sq & 7];
			eval += pwnConnect[Long.bitCount((BB.pawnConnect(0, sq) & white))];
			eval += +(1 & (int) ((1l << sq & bbInnerCenter) - 1) >> 63) * 10;
			eval += +3 * Bits.sparsePopCount(attacks & bbMiddleCenter);
			eval += 1 & ((int) ((((1l << sq) & blackHoles)) - 1) >> 63) * passedPawn[sq >>> 3];
			eval += 20 * Bits.sparsePopCount(BB.pawnConnect(0, sq) & white & blackHoles);
			return eval;
		case Board.BLACK_PAWN:
			eval = 80;
			attacks = BB.PAWN_ATTACKS[1][sq];
			eval += pawnPerLine[sq & 7];
			eval += pwnConnect[Long.bitCount((BB.pawnConnect(0, sq) & black))];
			eval += +(1 & (int) (((1l << sq) & bbInnerCenter) - 1) >> 63) * 10;
			eval += +3 * Bits.sparsePopCount(attacks & bbMiddleCenter);
			eval += 1 & ((int) ((((1l << sq) & whiteHoles)) - 1) >> 63) * passedPawn[7 - (sq >>> 3)];
			eval += 20 * Bits.sparsePopCount(BB.pawnConnect(0, sq) & black & whiteHoles);
			return eval;
		case Board.WHITE_KING:
			eval = 160;
			attacks = BB.queenAttacks(sq, white);
			attacks &= ~BB.RANKS[0];
			eval -= 2 * Long.bitCount(attacks);

			shield = whiteTrop[sq];
			eval -= 10 * Long.bitCount(shield & black);
			shield &= white;
			shield >>>= sq + 7;
			shield = shield >>> 5 | shield;
			shield &= 0x3f;
			eval += tropLup[(sq % 8) << 6 | (int) shield];
			return eval;
		case Board.BLACK_KING:
			eval = 160;
			attacks = BB.queenAttacks(sq, black);
			attacks &= ~BB.RANKS[56];
			eval -= 2 * Long.bitCount(attacks);
			shield = blackTrop[sq];
			eval -= 10 * Long.bitCount(shield & white);
			shield &= black;
			shield >>>= sq - 17;
			shield = shield >>> 5 | shield;
			shield &= 0x3f;
			eval += tropLup[1 << 9 | (sq % 8) << 6 | (int) shield];

			return eval;
		case Board.WHITE_QUEEN:
			attacks = BB.queenAttacks(sq, occ);
			return (90 + 2 * Long.bitCount(attacks));
		case Board.BLACK_QUEEN:
			attacks = BB.queenAttacks(sq, occ);
			return (90 + 2 * Long.bitCount(attacks));
		default:
			return 0;
		}
	}

	public boolean equals(ChessEval3 p) {
		return (white == p.white) && (black == p.black);
	}

	public double eval(Board b) {

		int eval = 0;
		long bb = 0l;
		int phase = b.getPhase();
		bb = b.getBitboard(Board.PIECES);
		bb &= ~occ;
		long whiteAttacks = 0l;
		long blackAttacks = 0l;
		long whiteKing = BB.kingAttacks(b.getKingSquare(0));
		long blackKing = BB.kingAttacks(b.getKingSquare(1));
		int dangerToWhite = 0;
		int dangerToBlack = 0;

		long atks = BB.pawnAttacks(white, 0);
		dangerToBlack += 2 * Long.bitCount(atks & blackKing);
		whiteAttacks |= atks;
		atks = BB.pawnAttacks(black, 1);
		dangerToWhite += 2 * Long.bitCount(atks & whiteKing);
		blackAttacks |= atks;
		long pieces = b.getPieces();
		int mobility = 0;
		long whiteTarget = b.getBitboard(Board.EMPTY) | b.getBitboard(Board.BLACK_PIECES);
		long blackTarget = b.getBitboard(Board.EMPTY) | b.getBitboard(Board.WHITE_PIECES);

		bb = b.getBitboard(Board.WHITE_KNIGHT);
		while (bb != 0) {
			int sq = Bits.bitscanForward(bb);
			bb &= bb - 1;
			atks = BB.KnightAttacks(sq);
			whiteAttacks |= atks;
			atks &= whiteTarget;
			dangerToBlack += 3 * Long.bitCount(atks & blackKing);
			mobility += Long.bitCount(atks);
		}

		bb = b.getBitboard(Board.BLACK_KNIGHT);
		while (bb != 0) {
			int sq = Bits.bitscanForward(bb);
			bb &= bb - 1;
			atks = BB.KnightAttacks(sq);
			blackAttacks |= atks;
			atks &= blackTarget;
			dangerToWhite += 3 * Long.bitCount(atks & whiteKing);
			mobility -= Long.bitCount(atks);
		}

		bb = b.getBitboard(Board.WHITE_BISHOP);
		while (bb != 0) {
			int sq = Bits.bitscanForward(bb);
			bb &= bb - 1;
			atks = BB.bishopAttacks(sq, pieces);
			whiteAttacks |= atks;
			atks &= whiteTarget;
			dangerToBlack += 3 * Long.bitCount(atks & blackKing);
			mobility += Long.bitCount(atks);

		}

		bb = b.getBitboard(Board.BLACK_BISHOP);
		while (bb != 0) {
			int sq = Bits.bitscanForward(bb);
			bb &= bb - 1;
			atks = BB.bishopAttacks(sq, pieces);
			blackAttacks |= atks;
			atks &= blackTarget;

			dangerToWhite += 3 * Long.bitCount(atks & whiteKing);
			mobility -= Long.bitCount(atks);

		}

		bb = b.getBitboard(Board.WHITE_ROOK);
		while (bb != 0) {
			int sq = Bits.bitscanForward(bb);
			bb &= bb - 1;
			atks = BB.rookAttacks(sq, pieces);
			whiteAttacks |= atks;
			atks &= whiteTarget;
			dangerToBlack += 3 * Long.bitCount(atks & blackKing);
			mobility += Long.bitCount(atks);

		}

		bb = b.getBitboard(Board.BLACK_ROOK);
		while (bb != 0) {
			int sq = Bits.bitscanForward(bb);
			bb &= bb - 1;
			atks = BB.rookAttacks(sq, pieces);
			blackAttacks |= atks;
			atks &= blackTarget;
			dangerToWhite += 3 * Long.bitCount(atks & whiteKing);
			mobility -= Long.bitCount(atks);

		}

		bb = b.getBitboard(Board.WHITE_QUEEN);
		while (bb != 0) {
			int sq = Bits.bitscanForward(bb);
			bb &= bb - 1;
			atks = BB.queenAttacks(sq, pieces);
			whiteAttacks |= atks;
			atks &= whiteTarget;
			dangerToBlack += 6 * Long.bitCount(atks & blackKing);
			mobility += Long.bitCount(atks);

		}

		bb = b.getBitboard(Board.BLACK_QUEEN);
		while (bb != 0) {
			int sq = Bits.bitscanForward(bb);
			bb &= bb - 1;
			atks = BB.queenAttacks(sq, pieces);
			blackAttacks |= atks;
			atks &= blackTarget;
			dangerToWhite += 6 * Long.bitCount(atks & whiteKing);
			mobility -= Long.bitCount(atks);
		}

		pieces &= ~occ;
		while (pieces != 0) {
			int sq = Bits.bitscanForward(pieces);
			pieces &= pieces - 1;
			eval += this.pieceEval(sq, b.getPiece(sq), phase);
		}

		atks = whiteKing & ~(blackAttacks | blackKing);
		int whiteKingMob = Long.bitCount(atks);
		mobility += whiteKingMob;
		atks = whiteKing & ~(whiteAttacks | whiteKing);
		int blackKingMob = Long.bitCount(atks);
		mobility -= blackKingMob;

		eval /= 24;
		eval += ((phase * this.eval) + (24 - phase) * this.eval) / 24;

		dangerToWhite *= dangerToWhite;
		dangerToBlack *= dangerToBlack;

		eval += (dangerToBlack * (24 - phase) + phase * dangerToBlack / (1 + blackKingMob)) / 24;
		eval -= (dangerToWhite * (24 - phase) + phase * dangerToWhite / (1 + whiteKingMob)) / 24;

		eval += 2 * mobility;

		long white = b.getBitboard(Board.WHITE_PIECES);
		long black = b.getBitboard(Board.BLACK_PIECES);
		long undefended = white & blackAttacks & ~whiteAttacks;
		undefended |= black & whiteAttacks & ~blackAttacks;

		int[] hanging = new int[2];

		while (undefended != 0) {
			int idx = Bits.bitscanForward(undefended);
			undefended &= undefended - 1;
			int piece = b.getPiece(idx);
			int delta = ChessEval3.undefended[piece];
			hanging[piece & 1] += delta;
		}

		eval += hanging[0] + hanging[1];
		hanging[0] = -hanging[0];
		if (b.isWhiteToMove())
			eval += 10;
		return eval / 10000d;
	}

	@Override
	public double applyAsDouble(ChessBoard value) {
		return new ChessEval3(value.getBoard()).eval(value.getBoard());
	}
}
