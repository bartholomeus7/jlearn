package chess;

import java.io.Serializable;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.stream.Collectors;

import chess.Chess.ChessBoard;
import chess.board.Castling;
import chess.board.ColoredPieceType;
import chess.board.PieceType;
import ml.FeatureFunction;

public class ChessFeature implements FeatureFunction<ChessBoard> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1880164525477304581L;
	private final static int FEATURE_LENGTH = 10000;
	private final List<Chess_SubFeature> subs;
	private final int feature_length;

	private static EnumMap<PieceType, Double> typeToDouble = new EnumMap<>(
			Arrays.stream(PieceType.values()).collect(Collectors.toMap(p -> p, p -> p.id * (1d / 6d))));

	public static interface Chess_SubFeature extends Serializable {
		int applyFeature(int current, double[] array, ChessBoard b);
	}

	public static enum ChessSubFeature implements Chess_SubFeature,Serializable {
		SIDE_TO_MOVE_FEATURE {
			@Override
			public int applyFeature(int current, double[] array, ChessBoard b) {
				array[current++] = b.getBoard().getCurrentColor();
				return current;
			}
		},
		CASTLING_RIGHT_FEATURE {
			public int applyFeature(int current, double[] array, ChessBoard b) {
				Castling[] castles = Castling.values;
				for (int i = 0; i < castles.length; i++) {
					array[current++] = b.getBoard().isAllowedCastle(castles[i]) ? 1 : 0;
				}
				return current;
			}
		},
		MATERIAL_CONFIG_FEATURE {
			public int applyFeature(int current, double[] array, ChessBoard b) {
				ColoredPieceType[] piecetypes = ColoredPieceType.REAL_PIECES;
				for (int i = 0; i < piecetypes.length; i++) {
					array[current++] = (double) b.getBoard().countPieces(piecetypes[i])
							/ piecetypes[i].type.start_count;
				}
				return current;
			}
		},

		ATTACK_AND_DEFEND_PER_SQUARE {

			@Override
			public int applyFeature(int current, double[] array, ChessBoard b) {
				for (int c = 0; c < 2; c++) {
					PieceType[] lowestAttacker = b.getAtkDefInfos().getLowestAttacker()[c];
					current = copy(lowestAttacker, array, current);
				}
				return current;
			}

			private int copy(PieceType[] pt, double[] array, int current) {
				for (int i = 0; i < pt.length; i++) {
					array[current++] = typeToDouble(pt[i]);
				}
				return current;
			}

		}
	}

	public ChessFeature(List<Chess_SubFeature> subFeatures) {
		subs = subFeatures;
		feature_length = testFeatureLength(subs);
	}

	private int testFeatureLength(List<Chess_SubFeature> features) {
		double[] test = new double[FEATURE_LENGTH];
		int current = 0;
		ChessBoard b = new ChessBoard();
		for (Chess_SubFeature f : features) {
			current = f.applyFeature(current, test, b);
		}
		return current;
	}

	@Override
	public double[] apply(ChessBoard t) {
		int current = 0;
		double[] result = new double[feature_length];
		for (Chess_SubFeature sf : subs) {
			current = sf.applyFeature(current, result, t);
		}
		if (current != feature_length) {
			throw new IllegalStateException("Illegal feature composition");
		}
		return result;
	}

	public static ChessFeature getDefaultFeature() {
		return new ChessFeature(Arrays.asList(ChessSubFeature.MATERIAL_CONFIG_FEATURE,
				ChessSubFeature.SIDE_TO_MOVE_FEATURE, ChessSubFeature.CASTLING_RIGHT_FEATURE,
				PieceListFeature.defaultPieceListFeaturues(), ChessSubFeature.ATTACK_AND_DEFEND_PER_SQUARE));
	}

	public static double typeToDouble(PieceType type) {
		if (type == null)
			return 0d;
		return typeToDouble.get(type);
	}

	@Override
	public int features() {
		return feature_length;
	}
}
