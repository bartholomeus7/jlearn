package chess;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;

import chess.Chess.ChessBoard;
import chess.board.ChessMove;
import chess.eval.TacticalEvaluator;
import chess.eval.MoveEvaluator;
import chess.mgen.AllMoveGenerator;
import chess.mgen.MoveGenerator;
import search.ABPlugin;
import search.FutilityPruning;
import search.NullMoveHeuristic;
import search.QSearchHeuristic;

public class ChessHeuristics {

	public static ABPlugin<ChessMove, ChessBoard> getQsceSearch() {
		return new QSearchHeuristic<ChessMove, ChessBoard>(getMoveGenerator(), b -> !b.getBoard().isCheck());
	}

	private static BiFunction<ChessBoard, Integer, List<ChessMove>> getMoveGenerator()

	{
		final MoveGenerator gen = new AllMoveGenerator();

		return (b, i) -> {
			if (b.getBoard().isCheck()) {
				return gen.getMoveList(b.getBoard());
			}
			final MoveEvaluator eval = new TacticalEvaluator(b.getBoard());
			List<ChessMove> result = new ArrayList<>(4);
			for (ChessMove m : gen.getMoveList(b.getBoard())) {
				if (m.isCapture(b.getBoard())) {
					eval.evalMove(m, b.getBoard());
					if (m.getEval() >= 0) {
						result.add(m);
					}
				}
			}
			Collections.sort(result);
			return result;
		};
	}

	public static ABPlugin<ChessMove, ChessBoard> getChessFutilityPruning(double margin) {

		return new FutilityPruning<ChessMove, ChessBoard>(margin,
				(m, b) -> !b.getBoard().isCheck() && !m.isCapture(b.getBoard()) && !m.givesCheck(b.getBoard()));
	}

	public static ABPlugin<ChessMove, ChessBoard> getNullMoveHeuristic() {
		return new NullMoveHeuristic<ChessMove, ChessBoard>(b -> b.getBoard().pushNullMove(),
				b -> b.getBoard().popNullMove(), b -> !b.getBoard().isCheck() && !b.getBoard().isKingAndPawnEndgame(),
				3);
	}

}
