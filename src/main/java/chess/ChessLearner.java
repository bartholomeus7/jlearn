package chess;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;

import ann.AdaDelta;
import ann.ErrorFunctions;
import ann.MultiLayerPerceptron;
import chess.Chess.ChessBoard;
import chess.board.Board;
import chess.board.ChessMove;
import ml.GameLearner;
import ml.NeuronalEval;
import search.ABEngine;
import search.KillerMoveHeuristic;
import search.SearchEngine;

public class ChessLearner {
	public static void main(String[] args) throws ParseException {
		MultiLayerPerceptron net = (MultiLayerPerceptron) new MultiLayerPerceptron(1).loadFromFile("chessMat3.net");
		net.setTrainer(new AdaDelta().setLearningRate(0.005d).setEpsilon(1e-7).setTrained(net));
		net.setErrorFunction(ErrorFunctions.LINEAR);
		Supplier<SearchEngine<ChessMove, ChessBoard, ?>> supplier = () -> chessEngine(
				new NeuronalEval<>(net, ChessFeature.getDefaultFeature()));

		GameLearner.Builder<ChessMove, ChessBoard> builder = new GameLearner.Builder<ChessMove, ChessBoard>(
				Chess.getInstance());
		builder.setFeature(ChessFeature.getDefaultFeature());
		builder.setNet(net);
		builder.setTestPositions(chessIterator());
		builder.setEngineSupplier(supplier);
		builder.setTester(StrategicTest.getInstance());
		GameLearner<ChessMove, ChessBoard> learner = builder.build();
		learner.run();
	}

	public static Iterable<ChessBoard> chessIterator() {
		return () -> {
			try {
				return Files.lines(Paths.get("positions.fen"), Charset.defaultCharset()).map(Board::new)
						.map(ChessBoard::new).iterator();
			} catch (IOException e) {
				throw new RuntimeException("Everything is shit");
			}

		};

	}

	private static SearchEngine<ChessMove, ChessBoard, ?> chessEngine(ToDoubleFunction<ChessBoard> eval) {
		ABEngine<ChessMove, ChessBoard> engine = new ABEngine<>(eval);
		engine.addPlugin(ChessHeuristics.getQsceSearch());
		engine.addPlugin(new GoodCaptureHeuristic());
		engine.addPlugin(new KillerMoveHeuristic<>());
		engine.addPlugin(new HistoryHeuristic());
		engine.setHashSize(1 << 16);
		engine.clear();
		// engine.addHeuristic(ChessHeuristics.getNullMoveHeuristic());
		return engine;
	}
}
