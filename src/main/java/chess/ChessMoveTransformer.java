package chess;

import chess.board.ChessMove;
import ml.MoveTransformer;

public class ChessMoveTransformer implements MoveTransformer<ChessMove> {

	private static double S = 1d / 7d;

	@Override
	public int length() {
		return 4;
	}

	private double coordToDouble(int x) {
		return x / 7d;
	}

	@Override
	public ChessMove parse(double[] array) {
		int from = (int) (array[0] / S) * 7 + (int) (array[1] / S);
		int to = (int) (array[2] / S) * 7 + (int) (array[3] / S);
		return new ChessMove(from, to, 0);
	}

	@Override
	public double[] serialize(ChessMove move) {
		double[] result = new double[4];
		result[0] = coordToDouble(move.getFrom() / 7);
		result[1] = coordToDouble(move.getFrom() % 7);
		result[2] = coordToDouble(move.getTo() / 7);
		result[3] = coordToDouble(move.getTo() % 7);
		return result;
	}

	@Override
	public double distance(ChessMove first, ChessMove second) {
		double result = (first.getTo() - second.getTo()) * (first.getTo() - second.getTo());
		result += (first.getFrom() - second.getFrom()) * (first.getFrom() - second.getFrom());
		return Math.sqrt(result);
	}

}
