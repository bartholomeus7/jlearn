package chess;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import chess.Chess.ChessBoard;
import chess.board.Board;
import chess.board.ChessMove;
import chess.eval.MoveEvaluator;
import search.ABEngine;
import search.ABPlugin;
import search.ABEngine.SearchSpot;
import search.SearchNode;

public class HistoryHeuristic implements ABPlugin<ChessMove, ChessBoard>, MoveEvaluator {

	private static Logger logger = Logger.getLogger(HistoryHeuristic.class);

	private int[][][] hhScore = new int[2][64][64];
	private int[][][] bfScore = new int[2][64][64];

	private void update(SearchNode<ChessMove, ChessBoard> ctxt) {
		Board b = ctxt.getBoard().getBoard();
		ChessMove move = ctxt.getLastMove();
		if (move == null) {
			logger.error("Null move in history heuristic");
			ctxt.getBoard().print();
		}
		if (!move.isCapture(b)) {
			int[][][] array = ctxt.isFailHigh() ? hhScore : bfScore;
			array[b.getCurrentColor()][move.getFrom()][move.getTo()]++;
		}
	}

	@Override
	public void increment() {
		hhScore = new int[2][64][64];
		bfScore = new int[2][64][64];
	}

	@Override
	public void register(ABEngine<ChessMove, ChessBoard> search) {
		search.addPlugin(this::update, SearchSpot.ON_CUTOFF);
		search.registerMoveSortingHeuristic(this::pushGoodHistoryMoves);
	}

	private int pushGoodHistoryMoves(SearchNode<ChessMove, ChessBoard> ctxt, List<ChessMove> moves) {
		Board board = ctxt.getBoard().getBoard();
		for (ChessMove m : moves) {
			this.evalMove(m, board);
		}
		Collections.sort(moves);
		return moves.size();
	}

	@Override
	public int evalMove(int move, Board b) {
		double hhscore = hhScore[b.getCurrentColor()][ChessMove.getFrom(move)][ChessMove.getTo(move)];
		double bfscore = bfScore[b.getCurrentColor()][ChessMove.getFrom(move)][ChessMove.getTo(move)];
		return (int) (hhscore * 1024 / (1d + bfscore));
	}

}
