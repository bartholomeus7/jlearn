package chess;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;

import chess.Chess.ChessBoard;
import chess.ChessFeature.Chess_SubFeature;
import chess.board.ColoredPieceType;
import chess.board.PieceType;
import chess.misc.Bits;

public class PieceListFeature implements Chess_SubFeature {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4655891810321984235L;
	private final List<PieceListSubFeature> subs;
	private final ToIntFunction<ColoredPieceType> bucketsPerType;

	public PieceListFeature(final List<PieceListSubFeature> subs) {
		this.subs = subs;

		this.bucketsPerType = new ToIntFunction<ColoredPieceType>() {

			int[] array = Arrays.stream(ColoredPieceType.REAL_PIECES)
					.mapToInt(cp -> subs.stream().filter(s -> s.appliesForType(cp.type)).mapToInt(s -> s.size()).sum())
					.toArray();

			@Override
			public int applyAsInt(ColoredPieceType value) {
				return array[value.boardIndex];
			}
		};
	}

	public static interface PieceListSubFeature extends Serializable {
		public int applyFeature(int current, double array[], ChessBoard b, int square, ColoredPieceType type);

		public boolean appliesForType(PieceType p);

		public int size();
	}

	public static PieceListSubFeature mobilitySubFeature(final Predicate<PieceType> appliesFor) {
		return new PieceListSubFeature() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 930403018666834044L;

			@Override
			public int applyFeature(int current, double[] array, ChessBoard b, int square, ColoredPieceType type) {
				array[current++] = (double) Long.bitCount(type.getAttacks(square, b.getBoard())) / 30d;
				return current;
			}

			@Override
			public boolean appliesForType(PieceType p) {
				return appliesFor.test(p);
			}

			@Override
			public int size() {
				return 1;
			}
		};
	}

	public static final PieceListSubFeature EXISTENCE_AND_POSITION = new PieceListSubFeature() {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5687242722567712695L;

		@Override
		public int applyFeature(int current, double[] array, ChessBoard b, int square, ColoredPieceType type) {
			array[current++] = 1d / (1d + (square / 8));
			array[current++] = 1d / (1d + (square % 8));
			array[current++] = 1d;
			return current;
		}

		@Override
		public boolean appliesForType(PieceType p) {
			return true;
		}

		@Override
		public int size() {
			return 3;
		}
	};

	public static final PieceListSubFeature ATTACKS_AND_DEFENDS = new PieceListSubFeature() {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public int applyFeature(int current, double[] array, ChessBoard b, int square, ColoredPieceType type) {
			array[current++] = ChessFeature
					.typeToDouble(b.getAtkDefInfos().getLowestAttacker()[type.color.index][square]);
			array[current++] = ChessFeature
					.typeToDouble(b.getAtkDefInfos().getLowestAttacker()[type.color.toggle().index][square]);
			return current;
		}

		@Override
		public boolean appliesForType(PieceType p) {
			return true;
		}

		@Override
		public int size() {
			return 2;
		}
	};

	@Override
	public int applyFeature(int current, double[] array, ChessBoard b) {
		for (ColoredPieceType cp : ColoredPieceType.REAL_PIECES) {
			int left = cp.type.start_count;
			long occ = b.getBoard().getOccurences(cp);
			left -= Long.bitCount(occ);
			left = Math.max(0, left);
			int maxTimes = cp.type.start_count;
			int times = 0;
			while (occ != 0 && times++ < maxTimes) {
				int sq = Bits.bitscanForward(occ);
				occ &= occ - 1;
				for (PieceListSubFeature sf : subs) {
					if (sf.appliesForType(cp.type)) {
						current = sf.applyFeature(current, array, b, sq, cp);
					}
				}

			}
			current += left * bucketsPerType.applyAsInt(cp);
		}
		return current;
	}

	public static PieceListFeature defaultPieceListFeaturues() {
		return new PieceListFeature(
				Arrays.asList(EXISTENCE_AND_POSITION, mobilitySubFeature(p -> p.slides), ATTACKS_AND_DEFENDS));
	}

}
