package chess;

import java.util.Collections;
import java.util.List;

import chess.Chess.ChessBoard;
import chess.board.Board;
import chess.board.ChessMove;
import chess.eval.TacticalEvaluator;
import chess.eval.MoveEvaluator;
import search.ABEngine;
import search.ABPlugin;
import search.SearchNode;

public class RemainingCaptureHeuristic implements ABPlugin<ChessMove, ChessBoard> {

	private MoveEvaluator eval(Board b) {

		return new MoveEvaluator() {
			TacticalEvaluator captEval = new TacticalEvaluator(b);

			@Override
			public int evalMove(int move, Board b) {
				if (ChessMove.isCapture(move, b))
					return captEval.evalMove(move, b);
				else
					return Integer.MIN_VALUE;
			}
		};
	}

	private int pushRemainingCaptures(SearchNode<ChessMove, ChessBoard> ctxt, List<ChessMove> moves) {
		Board board = ctxt.getBoard().getBoard();
		MoveEvaluator mev = eval(board);
		for (ChessMove m : moves) {
			mev.evalMove(m, board);
		}
		Collections.sort(moves);
		int count = 0;
		for (ChessMove m : moves) {
			if (m.getEval() > Integer.MIN_VALUE)
				count++;
			else
				break;
		}
		return count;
	}

	@Override
	public void register(ABEngine<ChessMove, ChessBoard> search) {
		search.registerMoveSortingHeuristic(this::pushRemainingCaptures);
	}

	@Override
	public void increment() {
	}

}
