package chess;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.function.Function;

import chess.Chess.ChessBoard;
import chess.board.ChessMove;
import chess.board.EPDBoard;

public class StrategicTest implements Function<Function<ChessBoard, ChessMove>, Double> {

	private static double MAX_POINTS = 13000d;
	private final List<EPDBoard> testBoards;
	private static StrategicTest instance;

	private StrategicTest() {
		try (InputStream is = StrategicTest.class.getResourceAsStream("/ressources/strategictest/STS.epd")) {
			testBoards = EPDBoard.listEPDBoards(is);
		} catch (IOException e) {
			throw new RuntimeException("Coult not init strategic test suite.", e);
		}
	}

	public double test(Function<ChessBoard, ChessMove> moveSelector) {
		return testBoards.parallelStream()
				.mapToInt(epd -> epd.scoreMove(moveSelector.apply(new ChessBoard(epd.getBoard())))).sum() / MAX_POINTS;
	}

	public static StrategicTest getInstance() {
		if (instance == null)
			instance = new StrategicTest();
		return instance;
	}

	@Override
	public Double apply(Function<ChessBoard, ChessMove> t) {
		return test(t);
	}
}
