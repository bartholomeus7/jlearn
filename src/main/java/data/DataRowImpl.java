package data;

import java.util.Arrays;

import data.DataSet.DataRow;

public class DataRowImpl implements DataRow {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8275157151965329174L;
	private final double[] input;
	private final double[] output;

	public DataRowImpl(double[] input, double[] output) {
		super();
		this.input = input;
		this.output = output;
	}

	@Override
	public double[] getInput() {
		return input;
	}

	@Override
	public double[] getOutput() {
		return output;
	}

	@Override
	public String toString() {
		return Arrays.toString(input) + " -> " + Arrays.toString(output);
	}

}