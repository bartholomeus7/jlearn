package data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import data.DataSet.DataRow;

public interface DataSet<X extends DataRow> extends Iterable<X>, Serializable

{

	public static interface DataRow extends Serializable {
		public double[] getInput();

		public double[] getOutput();
	}

	public int size();

	public X getRow(int idx);

	public void add(double[] in, double[] out);

	public void add(double[][] row);

	public Stream<X> stream();

	public static DataSet<? extends DataRow> generate(Supplier<double[]> input, Function<double[], double[]> map,
			int count) {
		DataSetImpl set = new DataSetImpl(count);
		IntStream.range(0, count).mapToObj(i -> input.get()).forEach(k -> set.add(k, map.apply(k)));
		return set;
	}

	public default List<DataSet<X>> split(int max) {
		List<DataSet<X>> result = new ArrayList<>();

		return result;
	}

	public default void add(DataSet<?> other) {
		for (int i = 0; i < other.size(); i++) {
			this.add(other.getRow(i).getInput(), other.getRow(i).getOutput());
		}
	}

}
