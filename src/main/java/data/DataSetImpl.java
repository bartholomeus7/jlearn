package data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class DataSetImpl implements DataSet<DataRowImpl> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8189940159681839195L;
	private final List<DataRowImpl> list;

	public DataSetImpl(int size) {
		list = new ArrayList<>(size);
	}

	public DataSetImpl() {
		list = new ArrayList<>();
	}

	@Override
	public int size() {
		return list.size();
	}

	@Override
	public DataRowImpl getRow(int idx) {
		return list.get(idx);
	}

	@Override
	public void add(double[] in, double[] out) {
		list.add(new DataRowImpl(in, out));
	}

	@Override
	public void add(double[][] row) {
		if (row.length != 2)
			throw new IllegalArgumentException("Outer array must be of size 2.");
		list.add(new DataRowImpl(row[0], row[1]));
	}

	@Override
	public Iterator<DataRowImpl> iterator() {
		return list.iterator();
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		for (DataRowImpl row : list) {
			b.append(row.toString()).append(System.lineSeparator());
		}
		return b.toString();
	}

	@Override
	public Stream<DataRowImpl> stream() {
		return list.stream();
	}

}
