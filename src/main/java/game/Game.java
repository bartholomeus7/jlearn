package game;

import org.apache.log4j.Logger;

public interface Game<M, B extends GameBoard<M, B>> {

	static Logger logger = Logger.getLogger(Game.class);

	public B buildStartingBoard();

	public B buildBoardFromString(String string);

}
