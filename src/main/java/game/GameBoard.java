package game;

import java.util.List;

import hash.LongHashable;

public interface GameBoard<M, B extends GameBoard<M, B>> extends LongHashable {

	public static enum GameState {
		PLAYER1_WON(true, 1d), PLAYER2_WON(true, 0d), DRAW(true, 0.5d), NO_END(false, Double.NaN);

		final boolean hasEnded;
		private final double player1Score;

		GameState(boolean ended, double player1Score) {
			this.hasEnded = ended;
			this.player1Score = player1Score;
		}

		public double getPlayer1Score() {
			if (this == NO_END)
				throw new IllegalStateException("No score can be assigned to a game that has not ended.");
			return player1Score;
		}

		public double getPlayer2Score() {
			return 1 - getPlayer1Score();
		}

	}

	public List<M> getMoves();

	public default boolean isFinished() {
		return getGameState().hasEnded;
	}

	public default void pushVariation(List<M> moves) {
		for (M m : moves) {
			this.pushMove(m);
		}
	}

	public default void popVariation(List<M> moves) {
		for (int i = moves.size() - 1; i >= 0; i--) {
			this.popMove(moves.get(i));
		}
	}

	public List<M> getVariation();

	public boolean isLegalMove(M move);

	public void pushMove(M m);

	public void popMove(M m);

	public B copy();

	public int getPlayerToMove();

	public GameState getGameState();

	public void print();

	public boolean isReversible();

	public boolean isRepetition();

	public Game<M, B> getGame();

}
