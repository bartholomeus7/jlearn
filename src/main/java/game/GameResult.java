package game;

public interface GameResult {

	public static enum MatchState {
		PLAYER1_TO_MOVE, PLAYER2_TO_MOVE, DRAW, PLAYER1_WON, PLAYER2_WON, RULE_VIALATION;
	}

}
