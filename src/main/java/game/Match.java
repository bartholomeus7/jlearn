package game;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Match<M, B extends GameBoard<M, B>> {

	private B board;
	private List<Player<M, B>> players = new ArrayList<>(2);

	public Match(Player<M, B> firstPlayer, Player<M, B> secondPlayer, B board) {
		this.board = board;
		players.add(firstPlayer);
		players.add(secondPlayer);
	}

	public B getCurrentBoard() {
		return board;
	}

	public void playNMoves(int n) {
		for (int i = 0; i < n; i++) {

		}
	}

	public GameResult playUntilEnd() {
		while (!board.isFinished()) {
			board.print();

			Player<M, B> currentPlayer = players.get(board.getPlayerToMove());
			new Thread(() -> currentPlayer.pushPlayerTurn(board, TimeUnit.NANOSECONDS.convert(1l, TimeUnit.SECONDS)))
					.run();
			try {
				Thread.sleep(1 * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();

			}
			System.out.println("here");
			M move = currentPlayer.getMove();
			System.out.println(move);
			board.pushMove(move);
			System.out.println("Player played move" + move);

		}
		return null;
	}

}
