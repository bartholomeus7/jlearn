package game;

import java.io.DataInputStream;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;

import search.Search;
import search.SearchEngine;

public interface Player<M, B extends GameBoard<M, B>> {
	public M getMove();

	public void pushPlayerTurn(B b, long nanosToPlay);

	public static <M, B extends GameBoard<M, B>> Player<M, B> getRandomPlayer() {
		return new Player<M, B>() {

			final Random r = new Random();
			private B board;

			@Override
			public M getMove() {
				List<M> moveList = board.getMoves();
				return moveList.get(r.nextInt(moveList.size()));
			}

			@Override
			public void pushPlayerTurn(B b, long nanosToPlay) {
				this.board = b;
			}
		};
	}

	public static <M, B extends GameBoard<M, B>> Player<M, B> getEnginePlayer(SearchEngine<M, B, ?> engine,
			long milisPerMove) {
		return new Player<M, B>() {

			private Search<M, B> search;

			@Override
			public M getMove() {
				search.abort();
				return search.getBestMove();
			}

			@Override
			public void pushPlayerTurn(B b, long nanosToPlay) {
				this.search = engine.buildSearch(b);
				search.setMaxSearchTime(nanosToPlay, TimeUnit.NANOSECONDS);
				search.run();
			}
		};
	}

	public static <M, B extends GameBoard<M, B>> Player<M, B> getConsolePlayer(
			final BiFunction<String, B, M> moveParser) {
		return new Player<M, B>() {

			private DataInputStream dis = new DataInputStream(System.in);
			M myMove = null;

			@Override
			public M getMove() {
				return myMove;
			}

			@SuppressWarnings("deprecation")
			// just ignore this one time please
			@Override
			public void pushPlayerTurn(B b, long nanosToPlay) {
				System.out.println("Player turns, milis to play : "
						+ TimeUnit.MILLISECONDS.convert(nanosToPlay, TimeUnit.NANOSECONDS));
				System.out.println("Current board:");
				b.print();
				while (true) {
					try {
						myMove = moveParser.apply(dis.readLine(), b);
						break;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		};
	}

}
