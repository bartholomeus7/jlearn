package hash;

public class EvalEntry implements LongHashable {

	private long hash;
	private double eval;

	@Override
	public long longhash() {
		return hash;
	}

	public double getEval() {
		return eval;
	}

	public EvalEntry(long hash, double eval) {
		super();
		this.hash = hash;
		this.eval = eval;
	}
	
	
}
