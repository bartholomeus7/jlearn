package hash;

import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.function.BiPredicate;

import org.apache.log4j.Logger;

import com.google.common.base.Function;

public class ReplacingHashMap<X extends LongHashable> {

	private static Logger logger = Logger.getLogger(ReplacingHashMap.class);

	private AtomicReferenceArray<X> table;

	// default is always replace an entry.
	private BiPredicate<X, X> replace = (x, y) -> true;

	private final int bmask;

	public ReplacingHashMap(int size) {
		if (size <= 0)
			throw new IllegalArgumentException("Size must be positive");
		// make size the next power of 2
		size = size % 2 == 0 ? size : (Integer.highestOneBit(size)) << 1;
		bmask = size - 1;
		table = new AtomicReferenceArray<>(size);
		logger.debug("Initialized hashtable with " + size + " entries.");
	}

	public ReplacingHashMap(int size, BiPredicate<X, X> replacementStrat) {
		this(size);
		this.replace = replacementStrat;
	}

	public void setReplacementStrategy(BiPredicate<X, X> replacementStrat) {
		this.replace = replacementStrat;
	}

	private int pos(LongHashable hashable) {
		return Long.hashCode(hashable.longhash()) & bmask;
	}

	public boolean store(X x) {
		// calculate hash pos via long hash->int hash-> % tablesize (via &
		// bitmask)
		int pos = pos(x);
		return x == table.accumulateAndGet(pos, x, (o, n) -> {
			if (replace.test(n, o)) {
				return n;
			} else
				return o;
		});
	}

	/**
	 * Gets an element in the hashtable if and only if the long hash equals to
	 * the longhash of the given argument. Returns null if no such element
	 * exists.
	 * 
	 * @param hashable
	 * @return
	 */

	public X get(LongHashable hashable) {
		X x = table.get(pos(hashable));
		if (x != null && x.longhash() == hashable.longhash())
			return x;
		else
			return null;
	}

	public <Y extends LongHashable> X computeIfAbsent(Y hashable, Function<Y, X> computor) {
		X result = get(hashable);
		if (result == null) {
			store(result = computor.apply(hashable));
		}
		return result;
	}

	public void clear() {
		table = new AtomicReferenceArray<>(table.length());
	}

}
