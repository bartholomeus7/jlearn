package hash;

import game.GameBoard;

public class ScoreEntry<M, B extends GameBoard<M, B>> implements LongHashable {

	public static enum ScoreType {
		TYPE_UPPERBOUND, TYPE_LOWERBOUND, TYPE_EXACT;
	}

	private long hash;

	@Override
	public String toString() {
		return "StdEntry [hash=" + hash + ", eval=" + eval + ", bestMove=" + bestMove + ", depth=" + depth + ", type="
				+ type + "]";
	}

	private double eval;
	private M bestMove;
	private int depth;
	private int age;
	private ScoreType type;

	@Override
	public long longhash() {
		return hash;
	}

	public ScoreEntry(long hash, double eval, M bestMove, int depth, ScoreType type, int age) {
		super();
		this.hash = hash;
		this.eval = eval;
		this.bestMove = bestMove;
		this.depth = depth;
		this.type = type;
		this.age = age;
	}

	public long getHash() {
		return hash;
	}

	public double getEval() {
		return eval;
	}

	public M getBestMove() {
		return bestMove;
	}

	public int getDepth() {
		return depth;
	}

	public ScoreType getType() {
		return type;
	}
	
	public int getAge(){
		return age;
	}

}
