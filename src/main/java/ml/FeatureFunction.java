package ml;

import java.io.Serializable;
import java.util.function.Function;

public interface FeatureFunction<X> extends Function<X, double[]>, Serializable {
	public int features();
}
