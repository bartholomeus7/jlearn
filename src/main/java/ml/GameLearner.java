package ml;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import org.apache.log4j.Logger;

import ann.AdaDelta;
import ann.ErrorFunctions;
import ann.MultiLayerPerceptron;
import ann.NeuralNetworkI;
import ann.TransferFunction;
import ann.WeightInits;
import data.DataSet;
import data.DataSetImpl;
import game.Game;
import game.GameBoard;
import search.ABEngine;
import search.KillerMoveHeuristic;
import search.Search;
import search.SearchEngine;

public class GameLearner<M, B extends GameBoard<M, B>> implements Runnable {

	private static Logger logger = Logger.getLogger(GameLearner.class);

	private final Game<M, B> game;
	private final ExecutorService executor;
	private final FeatureFunction<B> feature;
	private final Iterable<B> testPositions;
	private final NeuralNetworkI net;
	private final ThreadLocal<SearchEngine<M, B, ?>> threadsEngines;
	private final Function<Function<B, M>, Double> tester;
	@SuppressWarnings("unused")
	private final Function<B, Double> bootstrapper;
	private final int max_positions;
	private final int learning_batch_size;
	private final int td_depth;
	private final double td_discount;
	private final int epochs_per_test;
	private final BooleanSupplier convergence;

	private double besttest = Double.MIN_VALUE;

	private GameLearner(Builder<M, B> builder) {
		checkBuilder(builder);
		this.game = builder.getGame();
		this.executor = builder.getExecutor() == null ? new ForkJoinPool() : builder.getExecutor();
		this.feature = builder.getFeature();
		if (builder.getNet() != null)
			net = builder.getNet();
		else {
			int features = builder.getFeature().features();
			MultiLayerPerceptron net = new MultiLayerPerceptron(features, features, features, 1);
			net.setTrainer(new AdaDelta().setLearningRate(0.005d).setEpsilon(1e-7).setTrained(net));
			net.setTransferFunction(TransferFunction.RECTIFIED_LINEAR_UNIT);
			net.setTransferFunction(TransferFunction.TANH, net.layers());
			net.setErrorFunction(ErrorFunctions.LINEAR);
			net.initWeights(WeightInits.NULL);
			this.net = net;
		}
		if (builder.getEngineSupplier() != null) {
			this.threadsEngines = ThreadLocal.withInitial(builder.engineSupplier);
		} else {
			this.threadsEngines = ThreadLocal.withInitial(() -> {
				ABEngine<M, B> engine = new ABEngine<>(new NeuronalEval<>(this.net, this.feature));
				engine.addPlugin(new KillerMoveHeuristic<>());
				engine.setHashSize(1 << 16);
				engine.clear();
				return engine;
			});
		}
		this.testPositions = builder.getTestPositions() != null ? builder.getTestPositions() :

				getDefaultIterator(game);
		this.tester = builder.getTester();
		this.bootstrapper = builder.getBootstrapper();
		this.max_positions = builder.getPositions();
		this.learning_batch_size = builder.getLearningBatchSize();
		this.td_depth = builder.getTd_depth();
		this.td_discount = builder.getTd_discount();
		this.epochs_per_test = builder.getEpochs_per_test();
		this.convergence = builder.getConvergence();
	}

	private Iterable<B> getDefaultIterator(Game<M, B> game) {
		return () -> {
			return new Iterator<B>() {

				@Override
				public boolean hasNext() {
					return true;
				}

				@Override
				public B next() {
					Random r = new Random();
					B board = game.buildStartingBoard();
					List<M> movesPlayed = new ArrayList<>();
					SearchEngine<M, B, ?> engine = threadsEngines.get();
					while (!board.isFinished() && r.nextDouble() <= 0.99d) {
						M move = null;
						if (Math.random() < 0.05d) {
							List<M> moves = board.getMoves();
							move = moves.get(r.nextInt(moves.size()));
							continue;
						} else {
							Search<M, B> search = engine.buildSearch(board);
							search.setMaxSearchTime(5l, TimeUnit.MILLISECONDS);
							search.run();
							engine.clear();
							move = search.getBestMove();
						}
						if (move == null) {
							throw new IllegalStateException("No best move provided by search!");
						}
						board.pushMove(move);
						movesPlayed.add(move);
					}

					if (board.isFinished()) {
						for (int i = movesPlayed.size() - 1; i >= Math.max(0,
								movesPlayed.size() - 1 - r.nextInt(td_depth)); i--) {
							board.popMove(movesPlayed.get(i));
						}
					}

					return board;
				}
			};
		};
	}

	private void checkBuilder(Builder<M, B> builder) {
		if (builder.getFeature() == null)
			throw new IllegalArgumentException("Cannot learn without giving a feature function");
		if (builder.getNet() == null ^ builder.getEngineSupplier() == null) {
			throw new IllegalArgumentException(
					"You have to either provide both a neural network and an engine supplier or nothing");
		}
		if (builder.getLearningBatchSize() < 1)
			throw new IllegalArgumentException("batch sizes lower than 1 make no sense");
		if (builder.getTd_depth() < 1)
			throw new IllegalArgumentException("tp depthes lower than 1 make no sense");
		if (builder.getTd_discount() < 0 || builder.getTd_discount() > 1)
			throw new IllegalArgumentException("Choose td discount in interval [0,1].");
		if (builder.getTd_discount() < 0 || builder.getTd_discount() > 1)
			throw new IllegalArgumentException("Choose td discount in interval [0,1].");
		if (builder.getConvergence() == null)
			throw new IllegalArgumentException("Please choose convergence !=null, for example ()->false.");
	}

	public static class Builder<M, B extends GameBoard<M, B>> {

		private final Game<M, B> game;
		private ExecutorService executor;
		private FeatureFunction<B> feature;
		private Iterable<B> testPositions;
		private NeuralNetworkI net;
		private Supplier<SearchEngine<M, B, ?>> engineSupplier;
		private Function<Function<B, M>, Double> tester;
		private Function<B, Double> bootstrapper;
		private int positions = Integer.MAX_VALUE;
		private int learningBatchSize = 256;
		private int td_depth = 12;
		private double td_discount = 0.7d;
		private int epochs_per_test = 100;
		private BooleanSupplier convergence = () -> false;

		public Supplier<SearchEngine<M, B, ?>> getEngineSupplier() {
			return engineSupplier;
		}

		public Builder<M, B> setEngineSupplier(Supplier<SearchEngine<M, B, ?>> engineSupplier) {
			this.engineSupplier = engineSupplier;
			return this;
		}

		public int getEpochs_per_test() {
			return epochs_per_test;
		}

		public Builder<M, B> setEpochs_per_test(int epochs_per_test) {
			this.epochs_per_test = epochs_per_test;
			return this;
		}

		public BooleanSupplier getConvergence() {
			return convergence;
		}

		public Builder<M, B> setConvergence(BooleanSupplier convergence) {
			this.convergence = convergence;
			return this;
		}

		public int getLearningBatchSize() {
			return learningBatchSize;
		}

		public Builder(Game<M, B> game) {
			this.game = game;
		}

		public Game<M, B> getGame() {
			return game;
		}

		public ExecutorService getExecutor() {
			return executor;
		}

		public Builder<M, B> setExecutor(ExecutorService executor) {
			this.executor = executor;
			return this;
		}

		public FeatureFunction<B> getFeature() {
			return feature;
		}

		public Builder<M, B> setFeature(FeatureFunction<B> feature) {
			this.feature = feature;
			return this;
		}

		public Iterable<B> getTestPositions() {
			return testPositions;
		}

		public Builder<M, B> setTestPositions(Iterable<B> testPositions) {
			this.testPositions = testPositions;
			return this;
		}

		public NeuralNetworkI getNet() {
			return net;
		}

		public Builder<M, B> setNet(NeuralNetworkI net) {
			this.net = net;
			return this;
		}

		public Function<Function<B, M>, Double> getTester() {
			return tester;
		}

		public Builder<M, B> setTester(Function<Function<B, M>, Double> tester) {
			this.tester = tester;
			return this;
		}

		public Function<B, Double> getBootstrapper() {
			return bootstrapper;
		}

		public Builder<M, B> setBootstrapper(Function<B, Double> bootstrapper) {
			this.bootstrapper = bootstrapper;
			return this;
		}

		public int getPositions() {
			return positions;
		}

		public Builder<M, B> setPositions(int positions) {
			this.positions = positions;
			return this;
		}

		public int getsetLearningBatchSize() {
			return learningBatchSize;
		}

		public Builder<M, B> setLearningBatchSize(int learning) {
			this.learningBatchSize = learning;
			return this;
		}

		public int getTd_depth() {
			return td_depth;
		}

		public Builder<M, B> setTd_depth(int td_depth) {
			this.td_depth = td_depth;
			return this;
		}

		public double getTd_discount() {
			return td_discount;
		}

		public Builder<M, B> setTd_discount(double td_discount) {
			this.td_discount = td_discount;
			return this;
		}

		public GameLearner<M, B> build() {
			return new GameLearner<>(this);
		}

	}

	private Callable<DataSet<?>> getTdLeafTrainingSet(B board, int depth, double discount) {
		return new Callable<DataSet<?>>() {

			@Override
			public DataSet<?> call() throws Exception {
				SearchEngine<M, B, ?> engine = threadsEngines.get();
				DataSet<?> result = new DataSetImpl();
				List<Double> searchResults = new ArrayList<>(depth);
				List<double[]> features = new ArrayList<>();

				for (int i = 0; i < depth && !board.isFinished(); i++) {
					Search<M, B> search = engine.buildSearch(board);
					search.setMaxSearchTime(25l, TimeUnit.MILLISECONDS);
					search.run();
					engine.clear();

					B pv = board.copy();
					pv.pushVariation(search.getPricipalVariation());

					if (search.getBestMove() == null) {
						logger.warn("Best move is null");
						break;
					}
					searchResults.add(search.getEval());
					features.add(feature.apply(pv));
					board.pushMove(search.getBestMove());

					if (pv.isFinished() || pv.isRepetition()) {
						break;
					}
				}

				for (int i = features.size() - 1; i >= 0; i--) {
					double totalError = searchResults.get(i);
					for (int k = i; k < features.size() - 1; k++) {
						totalError += Math.pow(discount, k - i) * (searchResults.get(k + 1) - searchResults.get(k));
					}
					result.add(features.get(i), new double[] { totalError });
				}
				System.out.print("|");
				return result;
			}
		};
	}

	private double test(double best) {
		logger.info("Running test suit.");
		try {
			// we execute test in our tpool in order to enable streaming api to
			// go
			// parallel

			double testResult = executor.submit(() -> tester.apply(b -> {
				SearchEngine<M, B, ?> engine = threadsEngines.get();
				Search<M, B> search = engine.buildSearch(b);
				search.setMaxSearchTime(50l, TimeUnit.MILLISECONDS);
				search.run();
				return search.getBestMove();
			})).get();
			logger.info("TestResult: " + testResult);
			if (best < testResult) {
				net.saveToFile("best.net");
				return testResult;
			}
			logger.info("Best test:" + best);
			return best;
		} catch (InterruptedException | ExecutionException e1) {
			throw new RuntimeException("Could not execute test function.", e1);
		}
	}

	public NeuronalEval<M, B> learn() {
		Iterator<B> it = testPositions.iterator();
		NeuronalEval<M, B> eval = new NeuronalEval<>(net, feature);

		for (int c = 0; c < max_positions; c++) {

			if (c % epochs_per_test == 0 && tester != null) {
				logger.info("Running test suit.");
				besttest = test(besttest);
			}

			DataSet<?> data = new DataSetImpl(td_depth * learning_batch_size);
			List<Future<DataSet<?>>> result = new Vector<>(learning_batch_size);

			IntStream.range(0, learning_batch_size).mapToObj(i -> it.next())
					.forEach(b -> result.add(executor.submit(getTdLeafTrainingSet(b, td_depth, td_discount))));

			for (Future<DataSet<?>> e : result) {
				try {
					data.add(e.get());
				} catch (InterruptedException | ExecutionException e1) {
					logger.warn("Error on retrieving training dataset row.", e1);
				}
			}

			System.out.println("Training");
			net.train(data, true);
			logger.info("Trained a dataset.");

			if (c % 10 == 0) {
				System.out.print("Initial position eval: " + eval.applyAsDouble(game.buildStartingBoard()));
				
				net.saveToFile("spirit38.net");
				try {
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}

			if (convergence.getAsBoolean()) {
				logger.info("Convergence criteria reached, game learning is finished.");
				break;
			}

		}

		return eval;
	}

	@Override
	public void run() {
		learn();
	}

}
