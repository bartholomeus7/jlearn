package ml;

public interface MoveTransformer<M> {

	public int length();

	public M parse(double[] array);

	public double[] serialize(M move);

	public double distance(M first, M second);

}
