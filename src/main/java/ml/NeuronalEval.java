package ml;

import java.util.function.ToDoubleFunction;

import ann.NeuralNetworkI;
import game.GameBoard;

public class NeuronalEval<M, B extends GameBoard<M, B>> implements ToDoubleFunction<B> {

	private final NeuralNetworkI network;
	private final FeatureFunction<B> feature;

	public NeuronalEval(NeuralNetworkI eval, FeatureFunction<B> featureFunction) {
		this.network = eval;
		this.feature = featureFunction;
	}

	@Override
	public double applyAsDouble(B value) {
		return network.calculate(feature.apply(value))[0];
	}

}
