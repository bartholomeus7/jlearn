package search;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.ToDoubleFunction;

import game.GameBoard;
import hash.EvalEntry;
import hash.ReplacingHashMap;

public class ABEngine<M, B extends GameBoard<M, B>> implements SearchEngine<M, B, ABSearch<M, B>> {

	public static int DEFAULT_EVAL_HASH_SIZE = 1 << 18;

	private AtomicInteger flagCounter = new AtomicInteger();

	public static interface Flag_Manipulator<M, B extends GameBoard<M, B>> {
		public void set(SearchNode<M, B> node, boolean val);

		public default void set(SearchNode<M, B> node) {
			set(node, true);
		}

		public boolean get(SearchNode<M, B> node);
	}

	public enum SearchSpot {
		BEFORE_ITERATION, ON_CUTOFF, BEFORE_RETURN;
	}

	private final ToDoubleFunction<B> eval;

	private List<ABPlugin<M, B>> plugins = new ArrayList<>();
	private Map<SearchSpot, List<Consumer<SearchNode<M, B>>>> search_heuristics = new EnumMap<>(SearchSpot.class);

	private List<BiFunction<SearchNode<M, B>, List<M>, Integer>> move_heuristics = new ArrayList<>();
	private List<BiFunction<SearchNode<M, B>, M, Boolean>> move_prunings = new ArrayList<>();

	private final TTableHeuristic<M, B> ttable;
	// eval caching hashmap
	private final ReplacingHashMap<EvalEntry> evalTable = new ReplacingHashMap<EvalEntry>(DEFAULT_EVAL_HASH_SIZE);

	private ReentrantLock lock = new ReentrantLock();

	private void initPlugins() {
		for (SearchSpot s : SearchSpot.values()) {
			search_heuristics.put(s, new ArrayList<>());
		}
	}

	public ABEngine(ToDoubleFunction<B> eval) {
		this.eval = eval;
		initPlugins();
		addPlugin(ttable = new TTableHeuristic<>());
	}

	public ABEngine<M, B> addPlugin(ABPlugin<M, B> heuristic) {
		this.plugins.add(heuristic);
		heuristic.register(this);
		return this;
	}

	public void addPlugin(Consumer<SearchNode<M, B>> heurstic, SearchSpot spot) {
		search_heuristics.get(spot).add(heurstic);
	}

	public void registerMoveSortingHeuristic(BiFunction<SearchNode<M, B>, List<M>, Integer> mHeuristic) {
		move_heuristics.add(mHeuristic);
	}

	public void registerMovePruningHeuristic(BiFunction<SearchNode<M, B>, M, Boolean> pruning_heuristic) {
		this.move_prunings.add(pruning_heuristic);
	}

	public void increment() {
		plugins.forEach(ABPlugin::increment);
	}

	protected Iterable<Consumer<SearchNode<M, B>>> heuristics(SearchSpot spot) {
		return search_heuristics.get(spot);
	}

	public Iterable<BiFunction<SearchNode<M, B>, M, Boolean>> pruningHeuristics() {
		return move_prunings;
	}

	protected Iterable<M> moves(SearchNode<M, B> ctxt) {
		return () -> new Iterator<M>() {

			int h;
			int current = 0;
			int maxGenerated = 0;
			List<M> moves = ctxt.board.getMoves();

			@Override
			public boolean hasNext() {
				return current <= moves.size() - 1;
			}

			@Override
			public M next() {
				// do fire move heuristics as long there exists one not fired &
				// no heuristc-selected moves exist.
				while (current >= maxGenerated && h < move_heuristics.size() - 1) {
					maxGenerated += move_heuristics.get(h++).apply(ctxt, moves.subList(current, moves.size()));
				}
				return moves.get(current++);
			}
		};

	}

	protected double eval(B board) {
		double sgn = board.getPlayerToMove() == 0 ? 1 : -1;
		return evalTable.computeIfAbsent(board, b -> new EvalEntry(b.longhash(), sgn * eval.applyAsDouble(b)))
				.getEval();
	}

	protected void aquire(ABSearch<M, B> search) {
		lock.lock();
		search.registerSearchListener(new SearchListener<M, B>() {
			public void onSearchEnd() {
				increment();
				lock.unlock();
			};
		});
	}

	@Override
	public ABSearch<M, B> buildSearch(B board, int depth) {
		return new ABSearch<M, B>(board, depth, this);
	}

	public TTableHeuristic<M, B> getTtableHeuristic() {
		return ttable;
	}

	@Override
	public double staticEval(B board) {
		return eval(board);
	}

	public void setHashSize(int size) {
		this.ttable.setSize(size);
	}

	@Override
	public void clear() {
		plugins.forEach(h -> h.clear());
	}

	/**
	 * Due to efficiency reasons, heuristics can set flags ( bounded in number
	 * by 64) with the help of this function.
	 * 
	 * @return
	 */

	public Flag_Manipulator<M, B> getFlagManipulator() {
		if (flagCounter.get() >= 63)
			throw new IllegalStateException("Can not register more than 64 flags manipulators");
		return new Flag_Manipulator<M, B>() {
			final int flag_index = flagCounter.getAndIncrement();

			@Override
			public void set(SearchNode<M, B> node, boolean val) {
				if (val)
					node.setFlag(flag_index);
			}

			@Override
			public boolean get(SearchNode<M, B> node) {
				return node.flagIsSet(flag_index);
			}
		};
	}

}
