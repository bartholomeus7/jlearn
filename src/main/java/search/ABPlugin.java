package search;

import game.GameBoard;

public interface ABPlugin<M, B extends GameBoard<M, B>> {

	public void register(ABEngine<M, B> engine);

	public void increment();

	public default void clear() {
		increment();
	}

}
