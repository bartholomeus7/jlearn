package search;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import game.GameBoard;

public class ABSearch<M, B extends GameBoard<M, B>> implements Search<M, B> {

	private volatile M bestMove;
	private volatile List<M> principal_variation = new ArrayList<>();

	private volatile long nodesSearched = 0;
	private volatile long searchStart;
	private volatile long searchEnd;

	private volatile double eval = Double.NaN;

	private volatile int currentDepth = 0;
	private volatile int completedDepth = -1;

	private volatile boolean running = true;
	private final B board;

	private final int maxDepth;
	private final ABEngine<M, B> engine;

	public static final double DEFAULT_ALPHA = -1d - 1e-15;
	public static final double DEFAULT_BETA = 1d + 1e-15;

	private final List<SearchListener<M, B>> listeners = new ArrayList<>();
	private final CountDownLatch finished = new CountDownLatch(1);

	public ABSearch(B board, int maxDepth, ABEngine<M, B> engine) {
		super();
		this.board = board.copy();
		this.maxDepth = maxDepth;
		this.engine = engine;
	}

	@Override
	public void run() {
		informListeners(SearchListener::onSearchStartRequest);
		engine.aquire(this);
		this.searchStart = System.nanoTime();
		informListeners(SearchListener::onSearchStart);

		for (; currentDepth <= maxDepth && !isShuttingDown(); currentDepth++) {
			call();

			if (currentDepth > 0 && !isShuttingDown()) {
				if (!principal_variation.get(0).equals(bestMove)) {
					throw new IllegalStateException("Principal variation does not match with best move:"
							+ principal_variation + " vs " + bestMove);
				}
			}

			if (!this.isShuttingDown())
				completedDepth = currentDepth;

			informListeners(l -> l.onDepthReached(currentDepth));
		}

		searchEnd = System.nanoTime();
		informListeners(SearchListener::onSearchEnd);
		running = false;
		finished.countDown();
	}

	public void informListeners(Consumer<SearchListener<M, B>> action) {
		for (SearchListener<M, B> listener : listeners) {
			action.accept(listener);
		}
	}

	@Override
	public String toString() {
		return "ABSearchResult [bestMove=" + bestMove + ", nodesSearched=" + nodesSearched + ", milisSpend="
				+ TimeUnit.MILLISECONDS.convert(System.nanoTime() - searchStart, TimeUnit.NANOSECONDS) + ", eval="
				+ eval + " Pricipal_Variation=" + getPricipalVariation() + "]";
	}

	@Override
	public M getBestMove() {
		return bestMove;
	}

	@Override
	public long getNodesSearched() {
		return nodesSearched;
	}

	@Override
	public long getTimeSearched(TimeUnit unit) {
		if (searchStart == 0)
			return 0;
		if (running)
			return unit.convert(System.nanoTime() - searchStart, TimeUnit.NANOSECONDS);
		return unit.convert(searchEnd - searchStart, TimeUnit.NANOSECONDS);
	}

	@Override
	public double getEval() {
		return signum() * eval;
	}

	public SearchNode<M, B> call() {
		SearchNode<M, B> context = new SearchNode<M, B>(currentDepth, DEFAULT_ALPHA, DEFAULT_BETA, board.copy(), this,
				currentDepth);
		return context.alphabeta();
	}

	@Override
	public int getCurrentDepth() {
		return currentDepth;
	}

	@Override
	public B getPositionSearched() {
		return board;
	}

	@Override
	public void abort() {
		running = false;
		searchEnd = System.nanoTime();
		informListeners(SearchListener::onSearchAbort);
	}

	@Override
	public List<M> getPricipalVariation() {
		return principal_variation;
	}

	@Override
	public boolean isShuttingDown() {
		return !running && currentDepth > 1;
	}

	public void nodeSearched() {
		nodesSearched++;
	}

	protected void setBest(List<M> pv, double eval) {
		if (isShuttingDown()) {
			return;
		}
		if (pv.isEmpty()) {
			throw new IllegalArgumentException("No move provided");
		}
		M best = pv.get(0);
		if (best == null)
			throw new IllegalArgumentException("Cannot accept null move as best move");

		if (!best.equals(bestMove)) {
			informListeners(l -> l.onBestMoveChange(best));
		}
		if (eval != this.eval) {
			informListeners(l -> l.onEvalChange(signum() * eval));
		}
		this.bestMove = best;
		this.principal_variation = pv;
		this.eval = eval;

	}

	protected ABEngine<M, B> engine() {
		return engine;
	}

	@Override
	public void registerSearchListener(SearchListener<M, B> listener) {
		listeners.add(listener);
	}

	@Override
	public void unregisterSearchListener(SearchListener<M, B> listener) {
		listeners.remove(listener);
	}

	private double signum() {
		if (board.getPlayerToMove() == 0)
			return 1;
		else
			return -1;
	}

	@Override
	public void waitForTermination() {
		try {
			finished.await();
		} catch (InterruptedException e) {
			throw new RuntimeException("Error on waiting");
		}
	}

	@Override
	public int getCompletedDepth() {
		return completedDepth;
	}

	@Override
	public boolean PVIsPrecise() {
		B pv = board.copy();
		pv.pushVariation(this.principal_variation);
		double eval = engine.staticEval(pv);
		double sig = pv.getPlayerToMove() == 0 ? 1 : -1;
		return (sig * eval == getEval());
	}

}