package search;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import game.GameBoard;
import search.ABEngine.Flag_Manipulator;
import search.ABEngine.SearchSpot;

public class AspirationWindow<M, B extends GameBoard<M, B>> implements ABPlugin<M, B> {

	private List<Double> evals = new ArrayList<Double>();
	private final static double DEFAULT_MIN_WINDOW = 1e-3;

	// as all evalations should be inside [-1,1], max window size is 2.
	private static double MAX_WINDOW_SIZE = Math.abs(ABSearch.DEFAULT_ALPHA - ABSearch.DEFAULT_BETA);
	private Flag_Manipulator<M, B> aspiration_search = null;

	/**
	 * This class act according to the rule that all evalations should be inside
	 * [-1,1] interval.
	 * 
	 * @author fortknock
	 *
	 */
	private static class DoubleRange {
		private static double TOTAL_MIN = ABSearch.DEFAULT_ALPHA;
		private static double TOTAL_MAX = ABSearch.DEFAULT_BETA;
		private final double min;
		private final double max;

		DoubleRange(double min, double max) {
			this.min = Math.max(min, TOTAL_MIN);
			this.max = Math.min(max, TOTAL_MAX);
		}

		static DoubleRange getBallRange(double center, double radius) {
			return new DoubleRange(center - radius, center + radius);
		}

		public DoubleRange applyWidening(double widening, boolean isFailHigh) {
			return isFailHigh ? new DoubleRange(min, max + widening) : new DoubleRange(min - widening, max);
		}

		void applyToSearchNode(SearchNode<?, ?> node) {
			node.alpha = min;
			node.beta = max;
		}

		@Override
		public String toString() {
			return ("[" + min + "," + max + "]");
		}
	}

	/**
	 * Default widening strategy is based on the score difference of the two
	 * last evals.
	 */

	private static BiFunction<Integer, List<Double>, Double> DEFAULT_ASPIRATION_WINDOW_FUNCTION = (i, l) -> {
		if (l.size() <= 1)
			return MAX_WINDOW_SIZE;
		double dis = Math.abs(l.get(l.size() - 1) - l.get(l.size() - 2));
		return Math.max(DEFAULT_MIN_WINDOW, Math.min(1, Math.pow(2, i) * dis));
	};

	private final BiFunction<Integer, List<Double>, Double> widening_map;

	public AspirationWindow() {
		this(DEFAULT_ASPIRATION_WINDOW_FUNCTION);
	}

	public AspirationWindow(BiFunction<Integer, List<Double>, Double> widening_map) {
		this.widening_map = widening_map;
	}

	@Override
	public void register(ABEngine<M, B> search) {
		search.addPlugin(this::beforeIterate, SearchSpot.BEFORE_ITERATION);
		if (this.aspiration_search != null)
			throw new IllegalStateException("Cannot register heuristic to more than one search engine");
		this.aspiration_search = search.getFlagManipulator();
	}

	@Override
	public void increment() {
		evals = new ArrayList<Double>();
	}

	public void beforeIterate(SearchNode<M, B> node) {
		if (node.isRoot() && !aspiration_search.get(node)) {
			double lastEval = evals.isEmpty() ? 0 : evals.get(evals.size() - 1);
			DoubleRange current_window = DoubleRange.getBallRange(lastEval, widening_map.apply(1, evals));
			for (int i = 2; i < Integer.MAX_VALUE; i++) {
				SearchNode<M, B> window_search_node = node.copy();
				aspiration_search.set(window_search_node);
				current_window.applyToSearchNode(window_search_node);
				window_search_node.alphabeta();
				if (window_search_node.isPVNode() || node.search.isShuttingDown()) {
					evals.add(window_search_node.currentBest);
					// search was inside aspiration window, we can break now
					node.returnValue(window_search_node.currentBest);
					break;
				} else {
					// search was outside aspiration window, we have to widen
					// window now and research
					current_window = current_window.applyWidening(widening_map.apply(2, evals),
							window_search_node.isFailHigh());
				}
			}
		}
	}

}
