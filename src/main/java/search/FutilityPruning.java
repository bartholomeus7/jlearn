package search;

import java.util.function.BiFunction;

import game.GameBoard;

public class FutilityPruning<M, B extends GameBoard<M, B>> implements ABPlugin<M, B> {

	private final double futility_margin;
	private final BiFunction<M, B, Boolean> isPruneAllowed;

	public FutilityPruning(double futility_margin) {
		this.futility_margin = futility_margin;
		isPruneAllowed = (m, b) -> true;
	}

	public FutilityPruning(double futility_margin, BiFunction<M, B, Boolean> isPruneAllowed) {
		this.futility_margin = futility_margin;
		this.isPruneAllowed = isPruneAllowed;
	}

	@Override
	public void register(ABEngine<M, B> engine) {
		engine.registerMovePruningHeuristic(this::getsPruned);
	}

	private boolean getsPruned(SearchNode<M, B> node, M move) {
		if (node.isFrontierNode() && !isPruneAllowed.apply(move, node.board))
			return false;
		double eval = node.eval();
		return (eval + futility_margin <= node.alpha);
	}

	@Override
	public void increment() {
	}

}
