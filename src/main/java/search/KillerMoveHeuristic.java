package search;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import game.GameBoard;
import search.ABEngine.SearchSpot;

public class KillerMoveHeuristic<M, B extends GameBoard<M, B>> implements ABPlugin<M, B> {

	private static int START_SIZE = 20;

	private Object[][] killers = new Object[2][START_SIZE];

	@Override
	public void register(ABEngine<M, B> search) {
		search.addPlugin(this::afterCutOff, SearchSpot.ON_CUTOFF);
		search.registerMoveSortingHeuristic(this::pushKillerMoves);
	}

	private void ensureCapacity(int x) {
		if (killers.length < x) {
			killers = new Object[][] { Arrays.copyOf(killers[0], x + 5), Arrays.copyOf(killers[1], x + 5) };
		}
	}

	private void afterCutOff(SearchNode<M, B> ctxt) {
		int i = ctxt.pathlength();
		ensureCapacity(i);
		killers[1][i] = killers[0][i];
		killers[0][i] = ctxt.lastMove;
	}

	private int pushKillerMoves(SearchNode<M, B> ctxt, List<M> moves) {
		int killersResorted = 0;
		int p = ctxt.pathlength();
		ensureCapacity(p);
		int idx = 0;
		for (int i = 0; i < killers.length; i++) {
			Object killer = killers[i][p];
			if ((idx = moves.indexOf(killer)) > 0) {
				Collections.swap(moves, killersResorted++, idx);
			}
		}
		return killersResorted;
	}

	@Override
	public void increment() {
	}

	@Override
	public void clear() {
		killers = new Object[2][START_SIZE];
	}

}
