package search;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import ann.AdaDelta;
import ann.ErrorFunctions;
import ann.MultiLayerPerceptron;
import ann.NeuralNetworkI;
import ann.TransferFunction;
import data.DataSetImpl;
import game.GameBoard;
import ml.FeatureFunction;
import ml.MoveTransformer;
import search.ABEngine.SearchSpot;

public class MoveSelectiveLearner<M, B extends GameBoard<M, B>> implements ABPlugin<M, B> {

	private static Logger logger = Logger.getLogger(MoveSelectiveLearner.class);

	private final FeatureFunction<B> feature;
	private final MoveTransformer<M> transform;
	private final NeuralNetworkI net;

	int right = 0;
	int total = 0;

	private DataSetImpl data = new DataSetImpl();

	public MoveSelectiveLearner(FeatureFunction<B> feature, MoveTransformer<M> transformer) {
		this.feature = feature;
		this.transform = transformer;
		this.net = buildNetwork();
	}

	@Override
	public void register(ABEngine<M, B> search) {
		search.addPlugin(this::learnCuttingMove, SearchSpot.ON_CUTOFF);
		search.registerMoveSortingHeuristic(this::sortByNet);
	}

	private int sortByNet(SearchNode<M, B> node, List<M> moves) {
		M move = guessBestMove(node.board);
		Collections.sort(moves, (x, y) -> (int) (transform.distance(move, x) - transform.distance(move, y)));
		return moves.size();
	}

	private void learnCuttingMove(SearchNode<M, B> node) {

		if (node.isFailHigh() && Math.random() < 0.1d) {
			data.add(feature.apply(node.board), transform.serialize(node.bestMove));
			if (data.size() > 25) {
				logger.info("Training!");
				net.train(data, true);
				data = new DataSetImpl();
			}
		}

	}

	private M guessBestMove(B b) {
		M move = transform.parse(net.calculate(feature.apply(b)));
		List<M> list = b.getMoves();
		return Collections.max(list, (x, y) -> (int) (transform.distance(move, x) - transform.distance(move, y)));
	}

	private NeuralNetworkI buildNetwork() {
		MultiLayerPerceptron net = new MultiLayerPerceptron(feature.features(), 200, transform.length());
		net.setTransferFunction(TransferFunction.RECTIFIED_LINEAR_UNIT);
		net.setTransferFunction(TransferFunction.TANH, 1);
		net.setErrorFunction(ErrorFunctions.LINEAR);
		net.setTrainer(new AdaDelta().setTrained(net));
		return net;
	}

	@Override
	public void increment() {

	}

}
