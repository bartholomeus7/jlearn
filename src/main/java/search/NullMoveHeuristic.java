package search;

import java.util.function.Consumer;
import java.util.function.Predicate;

import game.GameBoard;
import search.ABEngine.Flag_Manipulator;
import search.ABEngine.SearchSpot;

public class NullMoveHeuristic<M, B extends GameBoard<M, B>> implements ABPlugin<M, B> {

	private static final double eps = 1e-4d;

	private final Consumer<B> pushNullMove;
	private final Consumer<B> popNullMove;
	private final Predicate<B> isNullMoveAllowed;
	private final int reduce;
	private Flag_Manipulator<M, B> nullmove_search;

	public NullMoveHeuristic(Consumer<B> pushNullMove, Consumer<B> popNullMove, Predicate<B> isNullAllowed,
			int reduce) {
		this.popNullMove = popNullMove;
		this.pushNullMove = pushNullMove;
		this.isNullMoveAllowed = isNullAllowed;
		this.reduce = reduce;
	}

	private void nullmove(SearchNode<M, B> node) {
		if (node.depthToGo >= 1 && node.getMovesPlayed() > 0 && isNullMoveAllowed.test(node.board)
				&& !nullmove_search.get(node)) {
			pushNullMove.accept(node.board);
			SearchNode<M, B> nullnode = nullmoveNode(node);
			nullnode.alphabeta();
			popNullMove.accept(node.board);
			if (nullnode.currentBest >= node.beta) {
				node.returnValue(node.beta);
			}
		}
	}

	private SearchNode<M, B> nullmoveNode(SearchNode<M, B> ctxt) {
		SearchNode<M, B> deeper = ctxt.deeper(reduce);
		deeper.alpha = deeper.beta + eps;
		nullmove_search.set(deeper);
		return deeper;
	}

	@Override
	public void register(ABEngine<M, B> search) {
		search.addPlugin(this::nullmove, SearchSpot.BEFORE_ITERATION);
		if (this.nullmove_search != null)
			throw new IllegalStateException("Cannot register heuristic to more than one search engine!");
		this.nullmove_search = search.getFlagManipulator();
	}

	@Override
	public void increment() {
		// heuristic is not stateful
	}

}
