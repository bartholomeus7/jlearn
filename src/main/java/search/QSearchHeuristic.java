package search;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;

import game.GameBoard;
import game.GameBoard.GameState;
import search.ABEngine.SearchSpot;

public class QSearchHeuristic<M, B extends GameBoard<M, B>> implements ABPlugin<M, B> {

	private final BiFunction<B, Integer, List<M>> moveSupplier;
	private final Predicate<B> isStandPatAllowed;

	public QSearchHeuristic(BiFunction<B, Integer, List<M>> moveSupplier, Predicate<B> isStandPatAllowed) {
		this.moveSupplier = moveSupplier;
		this.isStandPatAllowed = isStandPatAllowed;
	}

	class QSearchNode {
		protected final int depth;
		protected final int maxDepth;
		protected final B board;

		protected int nodesSearched = 0;
		protected M bestMove;
		protected double alpha;
		protected double beta;
		protected int movesPlayed = 0;

		protected List<M> pv = new ArrayList<>();
		protected final ABSearch<M, B> global;

		public QSearchNode(double alpha, double beta, int depth, B board, int maxDepth, ABSearch<M, B> global) {
			this.alpha = alpha;
			this.beta = beta;
			this.depth = depth;
			this.board = board;
			this.maxDepth = maxDepth;
			this.global = global;
		}

		public QSearchNode(SearchNode<M, B> leaf) {
			this(leaf.alpha, leaf.beta, 0, leaf.board, 0, leaf.search);
			assert (leaf.isLeaf()) : "Applying qsearch to non leaf node";
		}

		public QSearchNode deeper() {
			return new QSearchNode(-beta, -alpha, depth - 1, board, maxDepth, global);
		}

		public int pathlength() {
			return maxDepth - depth;
		}

		public double eval() {
			return eval(pathlength());
		}

		public double eval(int depthAdapt) {

			if (board.isFinished() || board.isRepetition()) {
				if (board.getGameState().equals(GameState.DRAW) || (board.isRepetition() && pathlength() > 1)) {
					return 0d;
				}
				return -1 + (depthAdapt + pathlength()) * SearchNode.WIN_DEPTH_EPSILON;
			}
			return global.engine().eval(board);
		}

		public void update(QSearchNode deeper, M move) {
			if (-deeper.alpha > beta) {
				alpha = beta;
			} else {
				if (-deeper.alpha > alpha) {
					pv.clear();
					pv.add(move);
					pv.addAll(deeper.pv);
					alpha = Math.max(alpha, -deeper.alpha);
				}
			}
		}

		public boolean isCutoff() {
			return alpha >= beta;
		}

		public QSearchNode qsearch() {
			if (global.isShuttingDown())
				return this;

			global.nodeSearched();

			if (board.isFinished()) {
				this.alpha = eval();
				return this;
			}

			if (isStandPatAllowed.test(board)) {
				double stand_pat = eval();
				if (stand_pat >= beta) {
					this.alpha = beta;
					return this;
				}

				if (alpha < stand_pat)
					alpha = stand_pat;
			}

			for (M m : moveSupplier.apply(board, Math.abs(depth))) {
				board.pushMove(m);
				update(deeper().qsearch(), m);
				board.popMove(m);
				if (isCutoff()) {
					return this;
				}
			}

			return this;
		}

	}

	private void applyQueisceSearch(SearchNode<M, B> node) {
		if (node.isLeaf()) {
			QSearchNode result = new QSearchNode(node).qsearch();
			double eval = result.alpha;
			node.returnValue(eval);
			node.pv = result.pv;
		}
	}

	@Override
	public void register(ABEngine<M, B> search) {
		search.addPlugin(this::applyQueisceSearch, SearchSpot.BEFORE_ITERATION);
	}

	@Override
	public void increment() {
		// heuristic is not stateful
	}

}
