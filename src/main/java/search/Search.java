package search;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import game.GameBoard;

public interface Search<M, B extends GameBoard<M, B>> extends Runnable {

	ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(1, new ThreadFactory() {
		public Thread newThread(Runnable r) {
			Thread t = Executors.defaultThreadFactory().newThread(r);
			t.setDaemon(true);
			return t;
		}
	});

	public B getPositionSearched();

	public M getBestMove();

	public long getNodesSearched();

	public long getTimeSearched(TimeUnit unit);

	public double getEval();

	public void abort();

	public List<M> getPricipalVariation();

	public boolean isShuttingDown();

	public void waitForTermination();

	public void registerSearchListener(SearchListener<M, B> listener);

	public void unregisterSearchListener(SearchListener<M, B> listener);

	public int getCurrentDepth();

	public int getCompletedDepth();

	public boolean PVIsPrecise();

	public default double getNodesPerSecond() {
		return (double) getNodesSearched() * 1e9 / getTimeSearched(TimeUnit.NANOSECONDS);
	}

	public default void setMaxSearchTime(long dur, TimeUnit unit) {
		registerSearchListener(new SearchListener<M, B>() {
			@Override
			public void onSearchStart() {
				SCHEDULER.schedule(() -> abort(), dur, unit);
			};

		});
	}

	public default void logSearch(Logger logger, Priority priority) {
		registerSearchListener(new SearchListener<M, B>() {
			public void onSearchStart() {
				logger.log(priority, "Search started.");
			};

			public void onSearchEnd() {
				logger.log(priority, "Search finished.");
			};

			public void onBestMoveChange(M m) {
				logger.log(priority, "Best move changed to " + m + " .");
			};

			public void onPricipialVariationChange() {
				logger.log(priority, "Principal variation changed to " + getPricipalVariation() + " .");
			};

			public void onEvalChange(double newEval) {
				logger.log(priority, "Eval changed to " + newEval + ".");
			};

			public void onDepthReached(int depthReached) {
				logger.log(priority, "Depth " + depthReached + " reached. Nodes calculate : " + getNodesSearched()
						+ " . Nodes per Second: " + getNodesPerSecond());
			}

			public void onSearchAbort() {
				logger.log(priority, "Search aborted.");
			}
		});
	}

}
