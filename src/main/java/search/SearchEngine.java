package search;

import game.GameBoard;

public interface SearchEngine<M, B extends GameBoard<M, B>, R extends Search<M, B>> {

	/**
	 * Builds a new search. A search engine can spawn arbitrary amounts of
	 * search instances. However, only one search must be active a time, locking
	 * the engine for the duration of the search. A search must only run one
	 * time. However, to reuse cached information, another search can be run on
	 * the same engine.
	 * 
	 * @param board
	 *            the position which shall be searched
	 * @param depth
	 *            the maximal depth to which the search shall be executed.
	 *            Different engine implementations might however have different
	 *            interpretations of 'depth" -therefore, it might be more
	 *            appropriate to limit the search timewise by calling
	 *            Search.setMaxSearchTime() or manually with Search.abort().
	 * @return
	 */

	public R buildSearch(B board, int depth);

	/**
	 * Builds a new search. A search engine can spawn arbitrary amounts of
	 * search instances. However, only one search must be active a time, locking
	 * the engine for the duration of the search. A search must only run one
	 * time. However, to reuse cached information, another search can be run on
	 * the same engine.
	 * 
	 * The returned search has no limits on how it will be run.
	 * 
	 * @param board
	 *            the position which shall be searched
	 * @return
	 */

	public default R buildSearch(B b) {
		return buildSearch(b, Integer.MAX_VALUE);
	}

	/**
	 * Increments all states in the sense that the position we like to search
	 * will differ from the previous one ('weak' clear). Each search engine has
	 * to guarantee that increment will be called before a new search will be
	 * executed (not built). 
	 */

	public void increment();

	/**
	 * Brings the engine back to the original state and deletes all caches.
	 */

	public void clear();

	/**
	 * Returns some static evaluation of the given board, thereby using no 'look
	 * ahead' and performing no searches.
	 * 
	 * @param board
	 * @return
	 */

	public default double staticEval(B board) {
		return buildSearch(board, 0).getEval();
	}

}
