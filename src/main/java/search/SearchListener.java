package search;

import game.GameBoard;

/**
 * Search listener is a listener interface which listens to search events.
 * 
 * @author fortknock
 *
 * @param <M>
 * @param <B>
 */

public interface SearchListener<M, B extends GameBoard<M, B>> {

	/**
	 * Gets triggered if an search attempts to start itself. However, he might
	 * have to wait for the engine to be ready.
	 */

	public default void onSearchStartRequest() {

	}

	/**
	 * Gets triggered when the search actuall starts to run.
	 */

	public default void onSearchStart() {
	};

	/**
	 * Gets triggered when the searh ended.
	 */

	public default void onSearchEnd() {
	};

	/**
	 * Gets triggerd as soon as the best move found by search changed.
	 * 
	 * @param m
	 */

	public default void onBestMoveChange(M m) {
	};

	/**
	 * Gets triggerd as soon as the principal variation changes.
	 * 
	 * @param m
	 */

	public default void onPricipialVariationChange() {
	};

	public default void onEvalChange(double newEval) {
	};

	public default void onDepthReached(int depthReached) {

	}

	public default void onNextMoveSearched(M move) {

	}

	public default void onSearchAbort() {

	}
}
