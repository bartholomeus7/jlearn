package search;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;

import game.GameBoard;
import game.GameBoard.GameState;
import search.ABEngine.SearchSpot;

public class SearchNode<M, B extends GameBoard<M, B>>

{

	public static final double WIN_DEPTH_EPSILON = 1e-8;

	protected final int depthToGo;
	protected final int maxDepth;

	protected final B board;

	protected int nodesSearched = 0;
	protected M bestMove;
	protected double alpha;
	protected double beta;
	protected double currentBest;
	protected double saveAlpha;
	protected M lastMove;
	protected int movesPlayed = 0;
	/**
	 * We try to be exact as possible about the principal variation, thereby we
	 * cache it in every search node.
	 */
	protected List<M> pv = new ArrayList<>();
	// a flag indiciating to the node is should quit alpha beta on the next
	// reasonable
	private boolean returnValue = false;
	/**
	 * A (bit) set of flags which can be used for heuristics to 'mark' node or
	 * transport certain information. flags can be accessed by asking for a flag
	 * manipulator at the search engine used.
	 */
	private long flags = 0l;

	/**
	 * we cache eval here. If no evaluation has been established until now, eval
	 * equals Double.NaN.
	 */
	private double eval = Double.NaN;
	/**
	 * A reference to the parent search this node is a child of.
	 */
	protected final ABSearch<M, B> search;

	public SearchNode(int depthToGo, double alpha, double beta, B board, ABSearch<M, B> global, int maxDepth) {
		super();
		this.depthToGo = depthToGo;
		this.alpha = alpha;
		this.beta = beta;
		this.currentBest = alpha;
		this.saveAlpha = alpha;
		this.board = board;
		this.search = global;
		this.maxDepth = maxDepth;
	}

	public boolean IsReturningValue() {
		return returnValue;
	}

	public void returnValueNow() {
		this.returnValue = true;
	}

	public SearchNode<M, B> deeper() {
		return new SearchNode<M, B>(depthToGo - 1, -beta, -currentBest, board, search, maxDepth);
	}

	public SearchNode<M, B> deeper(int deeper) {
		return new SearchNode<M, B>(depthToGo - deeper, -beta, -currentBest, board, search, maxDepth);
	}

	public SearchNode<M, B> copy() {
		return new SearchNode<M, B>(depthToGo, alpha, beta, board, search, maxDepth);
	}

	public boolean isLeaf() {
		return depthToGo <= 0;
	}

	public boolean isRoot() {
		return depthToGo == maxDepth;
	}

	public int getMovesPlayed() {
		return movesPlayed;
	}

	public void update(M move, double eval, List<M> deeperPV) {
		if (eval > currentBest) {
			currentBest = eval;
			bestMove = move;
			if (this.isPVNode()) {
				pv.clear();
				pv.add(move);
				pv.addAll(deeperPV);
				if (isRoot()) {
					search.setBest(this.pv, currentBest);
				}
			}
		}
	}

	public void update(SearchNode<M, B> deeper, M move) {
		lastMove = move;
		if (!stopped()) {
			update(lastMove, -deeper.currentBest, deeper.pv);
		}
		movesPlayed++;
		this.nodesSearched += deeper.nodesSearched;
	}

	public List<M> getPV() {
		return pv;
	}

	public SearchNode<M, B> returnValue(double val) {
		currentBest = val;
		returnValueNow();
		return this;
	}

	protected final void setFlag(int position) {
		flags |= 1l << position;
	}

	protected boolean flagIsSet(int position) {
		return ((flags >>> position) & 1l) != 0;
	}

	public int pathlength() {
		return maxDepth - depthToGo;
	}

	public boolean stopped() {
		return search.isShuttingDown();
	}

	public void nodeSearched() {
		search.nodeSearched();
		this.nodesSearched++;
	}

	public double eval() {
		return eval(0);
	}

	public List<M> getPrincipalVariation() {
		return pv;
	}

	public double eval(int depthAdapt) {
		// we cache evaluation, so check cache first
		if (!Double.isNaN(eval)) {
			return eval;
		}
		if (depthAdapt < 0)
			throw new IllegalArgumentException("depthAdapt must be non negative!");
		if (board.isFinished() || board.isRepetition()) {
			// we eval repetitions which do not occur at the root as draw to
			// avoid cycle stuff.
			if (board.getGameState().equals(GameState.DRAW) || (board.isRepetition() && pathlength() > 1)) {
				return eval = 0d;
			}
			return eval = -1 + (depthAdapt + pathlength()) * WIN_DEPTH_EPSILON;
		}
		return eval = search.engine().eval(board);
	}

	public B getBoard() {
		return board;
	}

	public boolean isMatePruned(double alpha, int pathLength) {
		return alpha > pathLength * WIN_DEPTH_EPSILON - 1;
	}

	public SearchNode<M, B> alphabeta() {
		nodeSearched();

		if (stopped())
			return returnValue(alpha);

		// in case of game has ended, evaluate immediately
		if (board.isFinished()) {
			return returnValue(eval());
		}

		if (isMatePruned(alpha, pathlength())) {
			// return returnValue(0d);
		}

		// Check heuristics before doing any further steps.This might enable us
		// to lookup the position in the transposition table / or endgame table
		// base.
		for (Consumer<SearchNode<M, B>> h : search.engine().heuristics(SearchSpot.BEFORE_ITERATION)) {
			h.accept(this);
			if (IsReturningValue()) {
				return this;
			}
		}
		// in case of leaf node, eval the board position
		if (isLeaf())
			return returnValue(search.engine().eval(board));

		for (M m : search.engine().moves(this)) {
			// check pruning heuristics whether move gets pruned
			// we only allow pruning if at least one move has been played
			if (movesPlayed >= 1 && getsPruned(m))
				continue;
			// otherwise continue minx max
			board.pushMove(m);
			update(deeper().alphabeta(), m);
			board.popMove(m);
			// in case of fail high, inform heuristics about occurrence of
			// cutoff
			if (isFailHigh()) {
				for (Consumer<SearchNode<M, B>> h : search.engine().heuristics(SearchSpot.ON_CUTOFF)) {
					h.accept(this);
				}
				break;
			}
		}

		assert (movesPlayed >= 1) : "Not one move was played";

		// we have to recheck here whether search has been stopped, otherwise
		// wrongly executed searches might influence heuristics
		if (stopped())
			return this;
		// inform heuristics about completion of local search tree
		for (Consumer<SearchNode<M, B>> h : search.engine().heuristics(SearchSpot.BEFORE_RETURN)) {
			h.accept(this);
		}

		return this;
	}

	private boolean getsPruned(M m) {
		for (BiFunction<SearchNode<M, B>, M, Boolean> pruning : search.engine().pruningHeuristics()) {
			if (pruning.apply(this, m))
				return true;
		}
		return false;
	}

	public boolean isFrontierNode() {
		return this.depthToGo == 1;
	}

	public M getLastMove() {
		return lastMove;
	}

	public boolean isFailLow() {
		return (currentBest <= saveAlpha);
	}

	public boolean isFailHigh() {
		return currentBest >= beta;
	}

	public boolean isPVNode() {
		return currentBest > saveAlpha && currentBest < beta;
	}

}