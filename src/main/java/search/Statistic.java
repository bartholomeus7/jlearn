package search;

import java.io.IOException;
import java.util.Arrays;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import game.GameBoard;
import search.ABEngine.SearchSpot;

public class Statistic<M, B extends GameBoard<M, B>> implements ABPlugin<M, B> {

	private final static String STATISTIC_LOG = "statistics.log";
	private static Logger logger = getStatisticLogger(STATISTIC_LOG);

	private static int MAX_DEPTH_IMAGANIBLE = 150;
	private static int MAX_MOVES_IMAGINABLE = 600;

	private static int PRINT_FREQ = 100000;
	private int count = 1;
	private int pvNode = 0;
	private int faillow = 0;
	private int failhigh = 0;

	private int[] cutoffs = new int[MAX_MOVES_IMAGINABLE];

	private int[] nodesPerDepth = new int[MAX_DEPTH_IMAGANIBLE];
	private int[] searchesPerformed = new int[MAX_DEPTH_IMAGANIBLE];

	private static Logger getStatisticLogger(String file) {
		Logger logger = Logger.getLogger(Statistic.class);
		FileAppender apndr;
		try {
			apndr = new FileAppender(new PatternLayout("%d %-5p [%c{1}] %m%n"), file, true);
			logger.addAppender(apndr);
			logger.setLevel((Level) Level.ALL);
			return logger;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		throw new IllegalStateException("Coult not open statistic log");

	}

	@Override
	public void register(ABEngine<M, B> search) {
		search.addPlugin(this::statistic, SearchSpot.BEFORE_RETURN);
	}

	private void statistic(SearchNode<M, B> node) {
		if (node.isFailHigh()) {
			cutoffs[node.getMovesPlayed()]++;
		}
		if (node.isFailLow())
			faillow++;
		else if (node.isFailHigh())
			failhigh++;
		else
			pvNode++;
		if (node.isRoot() && !node.stopped()) {
			nodesPerDepth[node.depthToGo] += node.nodesSearched;
			searchesPerformed[node.depthToGo]++;
		}
		if (count++ % PRINT_FREQ == 0) {
			logStatistics();
		}

	}

	private void logStatistics() {
		int sum = Arrays.stream(cutoffs).sum();
		for (int i = 1; i < 30; i++) {
			logger.info("Cutoff after " + i + " th move : " + cutoffs[i] * 100d / sum + " %.");
		}
		int sumNodes = faillow + failhigh + pvNode;
		logger.info("Fail low  " + 100d * faillow / sumNodes + " %");
		logger.info("Fail high  " + 100d * failhigh / sumNodes + " %");
		logger.info("PV Node  " + 100d * pvNode / sumNodes + " %");
		logger.info("Summed node " + sumNodes);
		int current = 1;
		while (searchesPerformed[current] > 0) {
			logger.info("Average nodes per depth " + current + " : "
					+ 1d * nodesPerDepth[current] / searchesPerformed[current]);
			current++;
		}
	}

	@Override
	public void increment() {
	}

}
