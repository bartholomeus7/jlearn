package search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.function.BiPredicate;

import org.apache.log4j.Logger;

import game.GameBoard;
import hash.ReplacingHashMap;
import hash.ScoreEntry;
import hash.ScoreEntry.ScoreType;
import search.ABEngine.SearchSpot;

/**
 * The transposition table heuristic is a heuristic inherently used by every
 * alpha beta search engine. It caches searched positions to memory, thereby saving any
 * properties of interest for later searches, which are:
 * 
 * Node Type: Fail low / Fail high / PV node <=> Score is lower bound / upper bound / exact
 * Eval : Eval figured out by search
 * Depth : Depth of the search tree done
 * Best move : Best move analyed by search.
 * 
 * 
 * 
 * @author fortknock
 *
 * @param <M>
 * @param <B>
 */

public class TTableHeuristic<M, B extends GameBoard<M, B>> implements ABPlugin<M, B> {

	private static int DEFAULT_SIZE = 1 << 18;

	private static Logger logger = Logger.getLogger(TTableHeuristic.class);
	private ReplacingHashMap<ScoreEntry<M, B>> ttable;
	private ScoreEntry<M, B> entry;
	private int size;
	private int age = 0;

	private BiPredicate<ScoreEntry<M, B>, ScoreEntry<M, B>> defaul_replacement_strategy = (n, o) -> {
		if (o == null)
			return true;
		if (n.getAge() > o.getAge())
			return true;
		if (n.getType() == ScoreType.TYPE_EXACT)
			return true;
		return n.getDepth() > o.getDepth();
	};

	public TTableHeuristic(int size) {
		ttable = new ReplacingHashMap<>(this.size = size, defaul_replacement_strategy);
	}

	public TTableHeuristic() {
		this(DEFAULT_SIZE);
	}

	@Override
	public void register(ABEngine<M, B> search) {
		search.addPlugin(this::lookupTable, SearchSpot.BEFORE_ITERATION);
		search.registerMoveSortingHeuristic(this::pushTTMove);
		search.addPlugin(this::storeResults, SearchSpot.BEFORE_RETURN);
	}

	private void lookupTable(SearchNode<M, B> ctxt) {
		entry = ttable.get(ctxt.board);
		if (entry != null && entry.getDepth() >= ctxt.depthToGo) {
			switch (entry.getType()) {
			case TYPE_LOWERBOUND:
				ctxt.alpha = Math.max(ctxt.alpha, entry.getEval());
				break;
			case TYPE_UPPERBOUND:
				ctxt.beta = Math.min(ctxt.beta, entry.getEval());
				break;
			case TYPE_EXACT:
				ctxt.update(entry.getBestMove(), entry.getEval(), Collections.emptyList());
				ctxt.returnValueNow();
			}
			if (ctxt.alpha >= ctxt.beta) {
				ctxt.returnValue(ctxt.alpha);
			}
		}
	}

	public ScoreEntry<M, B> getEntry(B board) {
		return this.ttable.get(board);
	}

	private void storeResults(SearchNode<M, B> ctxt) {
		if (ctxt.isFailLow()) {
			ttable.store(new ScoreEntry<M, B>(ctxt.board.longhash(), ctxt.currentBest, ctxt.bestMove, ctxt.depthToGo,
					ScoreType.TYPE_UPPERBOUND, age));
		} else if (ctxt.isFailHigh()) {
			ttable.store(new ScoreEntry<M, B>(ctxt.board.longhash(), ctxt.currentBest, ctxt.bestMove, ctxt.depthToGo,
					ScoreType.TYPE_LOWERBOUND, age));
		} else {
			if (ctxt.bestMove == null) {
				return;
			}
			ttable.store(new ScoreEntry<M, B>(ctxt.board.longhash(), ctxt.currentBest, ctxt.bestMove, ctxt.depthToGo,
					ScoreType.TYPE_EXACT, age));
		}
	}

	private int pushTTMove(SearchNode<M, B> ctxt, List<M> moves) {
		if (entry != null && entry.getHash() != ctxt.board.longhash()) {
			throw new IllegalStateException("Entry hash much match current board!");
		}
		if (entry != null && entry.getBestMove() != null) {
			int pos = moves.indexOf(entry.getBestMove());
			if (pos >= 0) {
				Collections.swap(moves, 0, pos);
				return 1;
			}
		}
		if (entry != null && entry.getBestMove() != null) {
			logger.warn("Found hash entry with move not contained in move list (-> hash collision.) ");
		}
		return 0;
	}

	public List<M> getPricipalVariation(B b) {
		List<M> result = new ArrayList<>();
		HashSet<Long> positions = new HashSet<>();
		ScoreEntry<M, B> entry = null;
		while ((entry = ttable.get(b)) != null && entry.getBestMove() != null && !positions.contains(b.longhash())) {
			positions.add(b.longhash());
			result.add(entry.getBestMove());
			b.pushMove(entry.getBestMove());
		}
		for (int i = result.size() - 1; i >= 0; i--) {
			b.popMove(result.get(i));
		}
		return result;
	}

	@Override
	public void increment() {
		age++;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public void clear() {
		ttable = new ReplacingHashMap<>(this.size, defaul_replacement_strategy);
	}

}
