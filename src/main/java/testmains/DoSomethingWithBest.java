package testmains;

import ann.MultiLayerPerceptron;
import ann.TransferFunction;

public class DoSomethingWithBest {
	public static void main(String[] args) {
		MultiLayerPerceptron net = (MultiLayerPerceptron) new MultiLayerPerceptron(1).loadFromFile("chessMat3.net");
		net.setTransferFunction(TransferFunction.BOUNDED_LINEAR, net.layers());
		net.saveToFile("chessMat3.net");
	}
}
