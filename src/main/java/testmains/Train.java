package testmains;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Stream;

import ann.MultiLayerPerceptron;
import ann.NeuralNetworkI;
import ann.TransferFunction;
import ann.WeightInits;
import chess.Chess.ChessBoard;
import chess.ChessEval;
import chess.ChessFeature;
import chess.board.Board;
import chess.mgen.CheckCaptureGenerator;
import chess.misc.IntList;
import data.DataRowImpl;
import data.DataSet;
import data.DataSet.DataRow;
import data.DataSetImpl;

public class Train {

	static {
		System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "4");
	}

	public static void main(String[] args) {
		try {
			train();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void train() throws Exception {
		MultiLayerPerceptron net = (MultiLayerPerceptron) getNet();
		// ChessFeature ml = new
		// ChessFeature(Arrays.asList(ChessSubFeature.MATERIAL_CONFIG_FEATURE));
		ChessFeature ml = ChessFeature.getDefaultFeature();
		trainForScore(net, ml, 0.0001d);
	}

	private static MultiLayerPerceptron getNet() {
		MultiLayerPerceptron net = new MultiLayerPerceptron(315, 315, 215, 1); //
		net.setTransferFunction(TransferFunction.RECTIFIED_LINEAR_UNIT);
		net.setTransferFunction(TransferFunction.TANH, net.layers());
		// net.setTrainer(new AdaGrad().setTrained(net));
		net.initWeights(WeightInits.GAUSSIAN);
		return net;
	}

	public static void train(NeuralNetworkI net, ChessFeature ml, double convergenceError) {
		final ChessEval eval = new ChessEval(new Board());
		// Function<ChessBoard, double[]> learn = b -> new double[] {
		// Math.tanh(15 * b.getBoard().getMaterial()) };
		Function<ChessBoard, double[]> learn2 = b -> new double[] { eval.applyAsDouble(b) };

		File posFile = new File("positions.fen");
		Iterable<ChessBoard> positions = () -> {
			try {
				return getStream(posFile, 40).iterator();
			} catch (Exception e) {
				throw new RuntimeException("FOFOOF");
			}
		};
		Iterator<ChessBoard> it = positions.iterator();
		DataSet<?> test = getNextDataSet(it, learn2, ml, 40);
		// ((MultiLayerPerceptron) net).printTests(test);
		int iterations = 0;
		double error = Double.MAX_VALUE;
		double maxError = Double.MAX_VALUE;
		// ((MultiLayerPerceptron) net).printTests(test);

		while ((error = net.getError(test)) > convergenceError) {
			// net.setLearningRate(net.getLearningRate() - 0.00025d);
			if (error < maxError) {
				maxError = error;
				net.saveToFile("best" + net.getIntputSize() + ".net");
			}
			DataSet<?> set = getNextDataSet(it, learn2, ml, 256);
			net.train(set, true);

			iterations++;
			if (iterations % 100 == 0) {
				System.out.println(
						"Error=" + error + " , Best_Error=" + maxError + " after " + iterations + " Iterations.");
			}
			if (iterations % 1000 == 0) {
				((MultiLayerPerceptron) net).printTests(test);
			}

		}
		net.saveToFile("best" + net.getIntputSize() + ".net");

		// System.out.println(net);

	}

	private static Stream<ChessBoard> getStream(File posFile) throws IOException {
		return Files.lines(posFile.toPath(), Charset.defaultCharset()).map(Board::new).map(ChessBoard::new);
	}

	private static DataSet<?> extract(Iterator<DataRowImpl> iterator, final int n) {
		DataSetImpl result = new DataSetImpl(n);
		int count = 0;
		while (count++ < n && iterator.hasNext()) {
			DataRow row = iterator.next();
			result.add(row.getInput(), row.getOutput());
		}
		return result;
	}

	public static void trainForScore(NeuralNetworkI net, ChessFeature ml, double convergenceError) throws IOException {
		File posFile = new File("scoredfens.fen");

		Iterator<DataRowImpl> it = getFenStream(posFile, 100).map(s -> {
			String[] split = s.split(";");
			ChessBoard b = new ChessBoard(new Board(split[0]));
			return new DataRowImpl(ml.apply(b), new double[] { Math.tanh(Integer.valueOf(split[1]) / 1000d) });
		}).iterator();

		// ((MultiLayerPerceptron) net).printTests(test);
		int iterations = 0;
		double error = Double.MAX_VALUE;
		double maxError = Double.MAX_VALUE;
		DataSet<?> test = extract(it, 256);

		// ((MultiLayerPerceptron) net).printTests(test);

		while ((error = net.getError(test)) > convergenceError) {
			// net.setLearningRate(net.getLearningRate() - 0.00025d);
			if (error < maxError) {
				maxError = error;
				net.saveToFile("best" + net.getIntputSize() + ".net");
			}
			DataSet<?> set = extract(it, 256);
			net.train(set, true);

			iterations++;
			if (iterations % 100 == 0) {
				System.out.println(
						"Error=" + error + " , Best_Error=" + maxError + " after " + iterations + " Iterations.");
			}
			if (iterations % 1000 == 0) {
				((MultiLayerPerceptron) net).printTests(test);
			}

		}
		net.saveToFile("best" + net.getIntputSize() + ".net");

		// System.out.println(net);

	}

	private static Stream<String> getFenStream(File posFile, final int concat) throws IOException {
		Stream<String> result = Files.lines(posFile.toPath());
		for (int i = 0; i < concat; i++) {
			result = Stream.concat(result, Files.lines(posFile.toPath()));
		}
		return result;
	}

	private static Stream<ChessBoard> getStream(File posFile, int concat) throws IOException {
		Stream<ChessBoard> result = getStream(posFile);
		for (int i = 0; i < concat; i++) {
			result = Stream.concat(result, getStream(posFile));
		}
		return result;
	}

	private static DataSet<?> getNextDataSet(Iterator<ChessBoard> it, Function<ChessBoard, double[]> target,
			ChessFeature feature, int size) {
		final CheckCaptureGenerator captGen = new CheckCaptureGenerator();
		final Random r = new Random();

		DataSetImpl data = new DataSetImpl(size);
		while (data.size() < size) {
			ChessBoard board = it.next();
			Board b = board.getBoard();
			if (!b.isCheck()) {
				IntList captures = captGen.generateMoves(b, new IntList());
				if (captures.size() > 0) {
					b.pushMove(captures.get(r.nextInt(captures.size())));
				}
			}
			double[] result = target.apply(board);
			if (result[0] != 0) {
				data.add(feature.apply(board), target.apply(board));
			}
		}

		return data;
	}
}
