package testmains;

import java.io.File;

import FourInARow.FourInARow;
import dis.RemoteVirtualMachine;

public class TrainRemotely {
	public static void main(String[] args) {
		try {
			remote();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void remote() throws Exception {
		RemoteVirtualMachine jvm = new RemoteVirtualMachine("fortknock", "192.168.1.2", Train.class);
		//jvm.setPassword(pw);
		jvm.uploadFile(new File("positions.fen"));
		jvm.uploadFile(new File("scoredfens.fen"));
		jvm.start();
	}

}
