package testmains;

import java.io.File;

import dis.RemoteVirtualMachine;
import ml.GameLearner;

public class TrainRemotely2 {
	public static void main(String[] args) {
		try {
			remote();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void remote() throws Exception {
		RemoteVirtualMachine jvm = new RemoteVirtualMachine("fortknock", "192.168.1.2", GameLearner.class);
		jvm.uploadFile(new File("fear.net"));
		jvm.uploadFile(new File("positions.fen"));
		jvm.uploadFile(new File("chessMat3.net"));
		jvm.uploadFile(new File("spirit34.net"));
		jvm.start();
	}

}
