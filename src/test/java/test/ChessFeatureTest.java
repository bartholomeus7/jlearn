package test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Iterator;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Test;

import ann.BackPropgation;
import ann.MultiLayerPerceptron;
import ann.NeuralNetworkI;
import ann.TransferFunction;
import chess.Chess.ChessBoard;
import chess.ChessFeature;
import chess.ChessFeature.ChessSubFeature;
import chess.board.Board;
import data.DataSet;
import data.DataSetImpl;

public class ChessFeatureTest {

	private Stream<ChessBoard> getStream(File posFile) throws IOException {
		return Files.lines(posFile.toPath(), Charset.defaultCharset()).map(Board::new).map(ChessBoard::new);
	}

	private Stream<ChessBoard> getStream(File posFile, int concat) throws IOException {
		Stream<ChessBoard> result = getStream(posFile);
		for (int i = 0; i < concat; i++) {
			result = Stream.concat(result, getStream(posFile));
		}
		return result;
	}

	@Test
	public void test() {
		MultiLayerPerceptron net = new MultiLayerPerceptron(12, 4, 1); //
		net.setTransferFunction(TransferFunction.TANH);
		net.setTrainer(new BackPropgation(net));
		// net.setTrainer(new AdaDelta(net));
		Function<ChessBoard, double[]> mat = b -> new double[] { 15 * b.getBoard().getMaterial() };
		ChessFeature ml = new ChessFeature(Arrays.asList(ChessSubFeature.MATERIAL_CONFIG_FEATURE));
		// ChessFeature ml = ChessFeature.getDefaultFeature();
		test(net, ml, mat, 0.0001d);
	}

	public void test(NeuralNetworkI net, ChessFeature ml, Function<ChessBoard, double[]> learn,
			double convergenceError) {
		// net.setTrainer(new BackPropgation(net));
		// net.setLearningRate(2000d);
		// net.setTransferFunction(TransferFunction.RECITIFER);
		File posFile = new File("/home/fortknock/workspace (2)/HarmonyChessEngine/positions.fen");
		Iterable<ChessBoard> positions = () -> {
			try {
				return getStream(posFile, 15).iterator();
			} catch (Exception e) {
				throw new RuntimeException("FOFOOF");
			}
		};
		Iterator<ChessBoard> it = positions.iterator();
		DataSet<?> test = getNextDataSet(it, learn, ml, 10);
		int iterations = 0;
		double error = Double.MAX_VALUE;
		double maxError = Double.MAX_VALUE;
		while ((error = net.getError(test)) > convergenceError) {
			// net.setLearningRate(net.getLearningRate() - 0.00025d);
			if (error < maxError) {
				maxError = error;
				net.saveToFile("best.net");
			}
			DataSet<?> set = getNextDataSet(it, learn, ml, 40);
			net.train(set, false);

			iterations++;
			if (iterations % 150 == 0) {
				System.out.println("Error=" + error);
				((MultiLayerPerceptron) net).printTests(test);
			}

		}

		((MultiLayerPerceptron) net).printTests(test);
	}

	private DataSet<?> getNextDataSet(Iterator<ChessBoard> it, Function<ChessBoard, double[]> target,
			ChessFeature feature, int size) {
		DataSetImpl data = new DataSetImpl(size);
		IntStream.range(0, size).mapToObj(i -> it.next()).forEach(d -> data.add(feature.apply(d), target.apply(d)));
		return data;
	}

}
