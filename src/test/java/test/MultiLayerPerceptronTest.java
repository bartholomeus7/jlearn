package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import ann.BackPropgation;
import ann.ErrorFunctions;
import ann.MultiLayerPerceptron;
import ann.TransferFunction;
import data.DataSet;
import data.DataSetImpl;

public class MultiLayerPerceptronTest {

	private static long STOP_TIME = 30 * 1000;
	private static double STOP_ERROR = 0.01d;

	@Test
	public void testXor() {
		MultiLayerPerceptron net = new MultiLayerPerceptron(2, 4, 1);
		net.setTrainer(new BackPropgation(net));
		// net.setTrainer(new AdaDelta(0.7, 1e-1d).setTrained(net));
		// net.setTransferFunction(TransferFunction.SIGMOID);
		net.setErrorFunction(ErrorFunctions.LINEAR);
		net.setTransferFunction(TransferFunction.CONTINUOUS_RECTIFIER);
		net.setTransferFunction(TransferFunction.TANH, net.layers());

		long startTime = System.currentTimeMillis();
		int iterations = 0;
		DataSet<?> xorSet = getXorSet();
		while (System.currentTimeMillis() - startTime < STOP_TIME) {
			net.train(xorSet, false);
			if (net.getError(getXorSet()) < STOP_ERROR) {
				break;
			}
			iterations++;
			if (iterations % 1000 == 0) {
				net.printTests(xorSet);
			}
			// foo.train(normalSet(), false);
			// System.out.println(foo);

		}
		System.out.println(
				"Iterations per second :" + ((double) iterations * 1000 / (System.currentTimeMillis() - startTime)));
		System.out.println("Training finished after " + iterations + " iterations.");
		assertTrue("Net did not converge after" + iterations + " iterations and 10 seconds.",
				net.getError(xorSet) < STOP_ERROR);
	}

	private static DataSet<?> getXorSet() {
		DataSetImpl trainingSet = new DataSetImpl(4);
		trainingSet.add(new double[] { 0, 0 }, new double[] { 0 });
		trainingSet.add(new double[] { 0, 1 }, new double[] { 1 });
		trainingSet.add(new double[] { 1, 0 }, new double[] { 1 });
		trainingSet.add(new double[] { 1, 1 }, new double[] { 0 });
		return trainingSet;
	}
}
